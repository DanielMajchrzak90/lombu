<?php

use Illuminate\Database\Seeder;
use App\Models\Lombard;
use App\Models\Agreement;
use App\Models\Product;

class DemoAgreementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lombards = Lombard::get();

        foreach ($lombards as $lombard) {   
            $agreementsPattern = $lombard->agreementsPatterns()->first();         
            factory(Agreement::class, 20)
                ->create([
                    'lombard_id' => $lombard->id,   
                    'agreement_pattern_id' => $agreementsPattern->id,
                ]);
        }
    }
}
