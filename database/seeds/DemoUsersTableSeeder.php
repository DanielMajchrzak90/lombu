<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class DemoUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)
            ->create([
                'email' => 'danielmajchrzak@onet.pl',
            ]);

        factory(User::class, 10)
            ->create();
    }
}
