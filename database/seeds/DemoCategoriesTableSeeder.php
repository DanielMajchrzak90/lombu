<?php

use Illuminate\Database\Seeder;
use App\Models\Lombard;
use App\Models\Category;

class DemoCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$lombards = Lombard::get();

		foreach ($lombards as $lombard) {
	    	factory(Category::class, 5)
				->create([
					'lombard_id' => $lombard->id,
				])
				->each(function ($c, $index) use ($lombard) {					
	                factory(Category::class, 3)
		                ->create([
							'lombard_id' => $lombard->id,
							'parent_id' => $c->id,
						])
						->each(function ($c, $index) use ($lombard) {
			                factory(Category::class, 2)
			                ->create([
								'lombard_id' => $lombard->id,
								'parent_id' => $c->id,
							]);
			            });
	            });
		}
    }
}
