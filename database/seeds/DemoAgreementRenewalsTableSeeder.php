<?php

use Illuminate\Database\Seeder;
use App\Models\Agreement;
use App\Models\AgreementRenewal;

class DemoAgreementRenewalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agreements = Agreement::get();

        foreach ($agreements as $agreement) {      
            factory(AgreementRenewal::class, 2)
                ->create([
                    'agreement_id' => $agreement->id, 
                ]);
        }
    }
}
