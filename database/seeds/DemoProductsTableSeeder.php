<?php

use Illuminate\Database\Seeder;
use App\Models\Lombard;
use App\Models\Product;

class DemoProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lombards = Lombard::get();

        foreach ($lombards as $lombard) {  

            for ($i=0; $i < 30; $i++) { 
        
                $category = $lombard->categories->shuffle()->first();
        
                factory(Product::class)
                    ->create([
                        'agreement_id' => null, 
                        'lombard_id' => $lombard->id, 
                        'order' => null,  
                        'category_id' => $category->id,
                    ]); 
            } 
        }
    }
}
