<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class DemoWorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereNull('employer_id')->get();

        foreach ($users as $user) {        	
	        factory(User::class, 5)
	            ->create([
	            	'employer_id' => $user->id,
	            ]);
        }
    }
}
