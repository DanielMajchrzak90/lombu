<?php

use Illuminate\Database\Seeder;

class DemoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DemoUsersTableSeeder::class);
        $this->call(DemoLombardsTableSeeder::class);
        $this->call(DemoCategoriesTableSeeder::class);
        $this->call(DemoAgreemetsPatternsTableSeeder::class);
        $this->call(DemoAgreementsTableSeeder::class);
        $this->call(DemoAgreementProductsTableSeeder::class);
        $this->call(DemoProductsTableSeeder::class);
        $this->call(DemoCustomersTableSeeder::class);
        $this->call(DemoWorkersTableSeeder::class);
        $this->call(DemoAgreementRenewalsTableSeeder::class);
    }
}
