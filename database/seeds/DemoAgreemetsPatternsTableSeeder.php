<?php

use Illuminate\Database\Seeder;
use App\Models\AgreementPattern;
use App\Models\Lombard;

class DemoAgreemetsPatternsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$lombards = Lombard::get();

    	foreach ($lombards as $lombard) {
    		factory(AgreementPattern::class, 1)->create([
				'lombard_id' => $lombard->id,	
			]);
    	}
    }
}
