<?php

use Illuminate\Database\Seeder;
use App\Models\Agreement;
use App\Models\Product;

class DemoAgreementProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agreements = Agreement::get();

        foreach ($agreements as $agreement) {     
                 
            for ($i=0; $i < 2; $i++) { 

                $category = $agreement->lombard->categories->shuffle()->first();   
                
                factory(Product::class)
                    ->create([
                        'agreement_id' => $agreement->id, 
                        'lombard_id' => $agreement->lombard_id, 
                        'order' => null,  
                        'category_id' => $category->id,
                    ]);
            }
        }
    }
}
