<?php

use Illuminate\Database\Seeder;
use App\Models\Lombard;
use App\Models\Customer;

class DemoCustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$lombards = Lombard::get();

    	foreach ($lombards as $lombard) {
    		factory(Customer::class, 15)->create([
				'lombard_id' => $lombard->id,	
			]);
    	}
    }
}
