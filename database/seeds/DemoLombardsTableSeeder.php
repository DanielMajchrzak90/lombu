<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Lombard;

class DemoLombardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::get();

        foreach ($users as $user) {        	
	        factory(Lombard::class, 2)
	            ->create([
	            	'user_id' => $user->id,
	            ]);
        }
    }
}
