<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agreement_id')->unsigned()->nullable();
            $table->integer('lombard_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->float('purchase_price', 8, 2)->nullable();
            $table->float('selling_price', 8, 2)->nullable();
            $table->float('market_price', 8, 2)->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
            
            $table->foreign('agreement_id')
                ->references('id')->on('agreements')->onDelete('set null');
            $table->foreign('lombard_id')
                ->references('id')->on('lombards')->onDelete('cascade');
            $table->foreign('category_id')
                ->references('id')->on('categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
