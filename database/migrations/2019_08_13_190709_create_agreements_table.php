<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Agreement;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lombard_id')->unsigned()->nullable();
            $table->integer('agreement_pattern_id')->unsigned()->nullable();
            $table->enum('type', [
                Agreement::LOAN_TYPE, 
                Agreement::SALE_TYPE, 
                Agreement::PURCHASE_TYPE, 
            ])->default(Agreement::LOAN_TYPE);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email', 100)->nullable();
            $table->string('pesel')->nullable();
            $table->string('id_number_and_series')->nullable();
            $table->text('options')->nullable();
            $table->date('end_at')->nullable();
            $table->float('amount', 8, 2)->nullable();
            $table->float('cost_per_day', 8, 2)->nullable();
            $table->timestamps();

            $table->foreign('lombard_id')
                ->references('id')->on('lombards')->onDelete('cascade');
            $table->foreign('agreement_pattern_id')
                ->references('id')->on('agreements_patterns')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
