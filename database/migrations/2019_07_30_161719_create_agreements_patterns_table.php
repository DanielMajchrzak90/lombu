<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementsPatternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements_patterns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lombard_id')->unsigned()->nullable();
            $table->string('name');
            $table->text('content');
            $table->timestamps();

            $table->foreign('lombard_id')
                ->references('id')->on('lombards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements_patterns');
    }
}
