<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementRenewalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_renewals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agreement_id')->unsigned()->nullable();
            $table->date('end_at')->nullable();
            $table->float('amount', 8, 2)->nullable();
            $table->timestamps();
            
            $table->foreign('agreement_id')
                ->references('id')->on('agreements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_renewals');
    }
}
