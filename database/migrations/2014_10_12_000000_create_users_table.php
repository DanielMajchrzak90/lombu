<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employer_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->string('confirmation_code')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->timestamp('password_updated_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('employer_id')
                ->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
