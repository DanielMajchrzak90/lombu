<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('pesel', 20);
            $table->string('id_number_and_series');
            $table->string('email')->nullable();
            $table->integer('lombard_id')->unsigned()->nullable();
            $table->timestamps();

            $table->unique(['lombard_id', 'pesel']);

            $table->foreign('lombard_id')
                ->references('id')->on('lombards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
