<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => 'secret',
        'remember_token' => str_random(10),
        'confirmed' => true,
        'confirmation_code' => null,
        'employer_id' => null,
    ];
});

$factory->define(App\Models\Lombard::class, function ($faker) {
	return [
		'name' => $faker->sentence(2),
        'user_id' => factory(App\Models\User::class),
    ];
});

$factory->define(App\Models\Category::class, function ($faker) {
    return [
        'name' => $faker->name,
        'slug' => str_limit($faker->unique()->slug, 50),
        'order' => 0,
        'lombard_id' => factory(App\Models\Lombard::class),
    ];
});

$factory->define(App\Models\Agreement::class, function ($faker) {
    $dt = $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years');
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'pesel' => $faker->numerify('###########'),
        'id_number_and_series' => str_random(6),
        'end_at' => $dt->format('Y-m-d'),
        'amount' => $faker->numberBetween(50, 1000),
        'cost_per_day' => 1,
        'lombard_id' => factory(App\Models\Lombard::class),
        'agreement_pattern_id' => factory(App\Models\AgreementPattern::class),
    ];
});

$factory->define(App\Models\Customer::class, function ($faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'pesel' => $faker->numerify('###########'),
        'id_number_and_series' => $faker->regexify('[A-Z]{3}[0-9]{3}'),
        'lombard_id' => factory(App\Models\Lombard::class),
    ];
});

$factory->define(App\Models\AgreementPattern::class, function ($faker) {
    return [
        'name' => $faker->sentence(2),
        'content' => view('_seed.agreement_pattern')->render(),
        'lombard_id' => factory(App\Models\Lombard::class),
    ];
});

$factory->define(App\Models\Product::class, function ($faker) {
    return [
        'name' => $faker->sentence(2),
        'description' => $faker->text(200),
        'agreement_id' => factory(App\Models\Agreement::class),        
        'lombard_id' => factory(App\Models\Lombard::class),        
        'category_id' => null,        
        'purchase_price' => $faker->randomNumber(2),
        'selling_price' => $faker->randomNumber(2),
        'market_price' => $faker->randomNumber(2),
        'order' => 0,
    ];
});

$factory->define(App\Models\AgreementRenewal::class, function ($faker) {
    $dt = $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years');
    return [
        'amount' => $faker->numberBetween(50, 1000),
        'end_at' => $dt->format('Y-m-d'),
        'agreement_id' => factory(App\Models\Agreement::class),
    ];
});
