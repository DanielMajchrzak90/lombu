<?php 

return [
	'register_login' => env('REGISTER_LOGIN', false),
	'test_seeder' => env('TEST_SEEDER', false),
];