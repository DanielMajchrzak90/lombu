<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Hash;
use App\Models\User;
use App\Models\Lombard;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        Validator::extend(
            'not_the_same_password',
            function ($attribute, $value, $parameters)
            {
                $user = User::where('email', $parameters[0])->first();
                
                if ($user) {
                    if (Hash::check($value, $user->password)) {
                        return false;
                    }
                }

                return true;
            }
        ); 
        Validator::extend(
            'check_password',
            function ($attribute, $value, $parameters)
            {
                $user = User::where('email', $parameters[0])->first();
                
                if ($user) {
                    if (Hash::check($value, $user->password)) {
                        return true;
                    }
                }

                return false;
            }
        ); 
        
        Validator::extend(
            'not_owner',
            function ($attribute, $value, $parameters)
            {
                $user = User::where('id', $parameters[0])->first();
                
                if ($user->email == $value) {
                    return false;
                }

                return true;
            }
        ); 
        
        Validator::extend(
            'not_added',
            function ($attribute, $value, $parameters)
            {
                $lombardId = $parameters[0];
                $user = Auth::user();
                $employee = User::where('email', $value)->first();
                $existsEmployee = null;

                if ($employee) {
                    $existsEmployee = Lombard::forUser($employee->id)
                        ->where('lombards.id', $lombardId)
                        ->first();
                }

                if ($existsEmployee) {
                    return false;
                }

                return true;
            }
        ); 

        Validator::extend(
            'in_content',
            function ($attribute, $value, $parameters, $validator)
            {   
                foreach ($parameters as $param) {
                    $param = ':'.$param;
                    if (!strpos($value, $param)) {

                    $validator->addReplacer(
                        'in_content', 
                        function ($message, $attribute, $rule, $parameters) use ($param) {
                            return str_replace(':field', $param, $message);
                        });

                        return false;
                    }   
                }

                return true;
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SortUrl', function($app) {
            return new \Acme\Helpers\SortUrl($app->make('request'));
        });
    }
}
