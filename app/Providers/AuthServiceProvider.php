<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Acme\Auth\JwtGuard;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Models\Agreement::class => \App\Policies\AgreementPolicy::class,
        \App\Models\Lombard::class => \App\Policies\LombardPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::extend('jwt', function ($app, $name, array $config) {
            return new JwtGuard(
                Auth::createUserProvider($config['provider'])
            );
        });

        Gate::define('isEmployer', function($user) {
            return $user->is_employer;
        });

        Gate::define('isWorker', function($user) {
            return $user->is_worker;
        });
    }
}
