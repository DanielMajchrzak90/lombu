<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Acme\Observers\UserObserver;
use Acme\Observers\CategoryObserver;
use Acme\Observers\FileObserver;
use Acme\Observers\AgreementObserver;
use Acme\Observers\LombardObserver;
use App\Models\User;
use App\Models\Category;
use App\Models\File;
use App\Models\Agreement;
use App\Models\Lombard;

class ObserversServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Category::observe(CategoryObserver::class);
        File::observe(FileObserver::class);
        Agreement::observe(AgreementObserver::class);
        Lombard::observe(LombardObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
