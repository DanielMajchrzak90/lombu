<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Auth;
use App\Models\Lombard;
use App\Models\Agreement;
use App\Exceptions\CustomAuthorizationException;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('lombard', function ($id) {

            $user = Auth::user();

            $lombard =  $user->lombards()->findOrFail($id);

            return $lombard;
        });

        Route::bind('worker', function ($id) {

            $user = Auth::user();

            $worker =  $user->workers()->findOrFail($id);

            return $worker;
        });

        Route::bind('employee', function ($id, $route) {

            if (!$route->lombard)
                return;

            $employee = $route->lombard->employees()->findOrFail($id);

            return $employee;
        });

        Route::bind('agreement_pattern', function ($id, $route) {
            if (!$route->lombard)
                return;
            
            $agreementPattern = $route->lombard->agreementsPatterns()->findOrFail($id);

            return $agreementPattern;
        });

        Route::bind('category', function ($id, $route) {

            if (!$route->lombard)
                return;
            
            $category = $route->lombard->categories()->findOrFail($id);

            return $category;
        });

        Route::bind('agreement', function ($id, $route) {

            if (!$route->lombard)
                return;
            
            $agreement = $route->lombard->agreements()->findOrFail($id);

            return $agreement;
        });

        Route::bind('renewal', function ($id, $route) {

            if (!$route->agreement)
                return;
            
            $renewal = $route->agreement->renewals()->findOrFail($id);

            return $renewal;
        });

        Route::bind('agreement_preview', function ($id, $route) {        
            $agreement = Agreement::findOrFail($id);

            return $agreement;
        });

        Route::bind('product', function ($id, $route) {

            if (!$route->lombard)
                return;
            
            $product = $route->lombard->products()->findOrFail($id);

            return $product;
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapFilesRoutes();

        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'prefix' =>  \LaravelLocalization::setLocale(),
            'middleware' => [ 
                'web',
                'localeSessionRedirect', 
                'localizationRedirect', 
            ],
        ], function() {     
            \App::setLocale(\LaravelLocalization::getCurrentLocale());
            Route::middleware([])->namespace($this->namespace)
                 ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapFilesRoutes()
    {
        Route::prefix('files')
             ->namespace($this->namespace.'\\Files')
             ->group(base_path('routes/files.php'));
    }
}
