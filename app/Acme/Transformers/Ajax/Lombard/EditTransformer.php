<?php

namespace Acme\Transformers\Ajax\Lombard;

use App\Models\Lombard;

class EditTransformer
{
	public function transform(Lombard $lombard)
	{
		return $lombard->only([
	        'name', 
	        'street', 
	        'street_no', 
	        'flat_no',
	        'postal_code',
	        'city', 
	        'user_id',
			'address', 
			'is_owner',
	    ]);
	}
}