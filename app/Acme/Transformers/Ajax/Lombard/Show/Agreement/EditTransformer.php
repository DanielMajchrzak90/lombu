<?php

namespace Acme\Transformers\Ajax\Lombard\Show\Agreement;

use App\Models\Agreement;
use App\Models\User;
use Illuminate\Support\Arr;

class EditTransformer
{
	public function transform(Agreement $agreement)
	{
		return array_merge(
			$agreement->only([
				'first_name', 
				'last_name', 
				'full_name', 
				'email', 
				'pesel',
				'id_number_and_series',
				'end_at',
				'amount',
				'cost_per_day',
				'url',
				'agreement_pattern_id',
			]), 
			[
				'products' => $agreement->products()->orderBy('order')->get()->map(function($item) {
					return $item->only(['name', 'market_price', 'order']);
				}),
			]
		);
	}
}