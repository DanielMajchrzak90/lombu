<?php

namespace Acme\Transformers\Ajax\Lombard\Show\Agreement;

use App\Models\Agreement;
use App\Models\User;

class IndexTransformer
{
	public function transform(Agreement $agreement)
	{
		return $agreement->only([
			'full_name', 
			'email', 
			'pesel',
			'id_number_and_series',
			'end_at',
			'amount',
			'created_at' => 'created_at_formatted',
			'preview_url',
		]);
	}
}