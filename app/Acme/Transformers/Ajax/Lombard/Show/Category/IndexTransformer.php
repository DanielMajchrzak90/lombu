<?php

namespace Acme\Transformers\Ajax\Lombard\Show\Category;

use App\Models\Category;
use App\Models\User;

class IndexTransformer
{
	public function transform(Category $category)
	{
		return $category->only(['name']);
	}
}