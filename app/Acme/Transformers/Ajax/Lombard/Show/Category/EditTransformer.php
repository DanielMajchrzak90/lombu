<?php

namespace Acme\Transformers\Ajax\Lombard\Show\Category;

use App\Models\Category;
use App\Models\User;

class EditTransformer
{
	public function transform(Category $category)
	{
		return $category->only(['name', 'parent_id']);
	}
}