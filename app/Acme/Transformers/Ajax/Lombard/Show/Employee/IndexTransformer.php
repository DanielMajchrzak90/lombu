<?php

namespace Acme\Transformers\Ajax\Lombard\Show\Employee;

use App\Models\Employee;
use App\Models\User;

class IndexTransformer
{
	public function transform(Employee $employee)
	{
		$user = $employee->user;

		return [
			'id' => $employee->id,
			'name' => $user->name,
			'email' => $user->email,
			'permissions' => $employee->permissions,
		];
	}
}