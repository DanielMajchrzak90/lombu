<?php

namespace Acme\Transformers\Ajax\Lombard\Show\AgreementPattern;

use App\Models\AgreementPattern;
use App\Models\User;

class IndexTransformer
{
	public function transform(AgreementPattern $agreementPattern)
	{
		return $agreementPattern->only(['name', 'content', 'preview_url']);
	}
}