<?php

namespace Acme\Transformers\Ajax\Lombard;

use App\Models\Lombard;

class ShowTransformer
{
	public function transform(Lombard $lombard)
	{
		return $lombard->only(['name', 'address', 'is_owner']);
	}
}