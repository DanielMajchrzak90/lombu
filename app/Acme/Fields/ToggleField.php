<?php

namespace Acme\Fields;

class ToggleField extends Field
{
    protected $component = 'toggle-field-component';

	protected function add($component)
	{
		$this->children[] = $component;
	}
}