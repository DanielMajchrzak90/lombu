<?php

namespace Acme\Fields;

class SelectField extends Field
{
	protected $component = 'select-field';
	
    protected $attributes = [
		'value_field' => 'id',
		'label_field' => 'name',
        'col' => ['col-md-12'],
    ];

	protected function options(array $options)
	{
        $this->setAttributes([
            'options' => $options,
        ]);
	}
}
