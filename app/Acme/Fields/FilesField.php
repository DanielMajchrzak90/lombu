<?php

namespace Acme\Fields;

class FilesField extends Field
{
    protected $component = 'files-field';
    protected $attributes = [
        'type' => 'file',
        'col' => ['col-md-12'],
    ];

	protected function placeholder($placeholder)
	{
        $this->setAttributes([
            'placeholder' => $placeholder,
        ]);
	}
}