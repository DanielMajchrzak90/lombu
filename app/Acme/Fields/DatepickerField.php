<?php

namespace Acme\Fields;

class DatepickerField extends Field
{
	protected $component = 'datepicker-field';

	protected function format($format)
	{
        $this->setAttributes([
            'format' => $format,
        ]);
	}
}