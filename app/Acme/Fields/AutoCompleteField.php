<?php

namespace Acme\Fields;

class AutoCompleteField extends TextField
{
	protected $component = 'auto-complete-field';
	
    protected $attributes = [
    	'emit_name' => 'autocomplete-selected',
		'value_field' => 'id',
		'label_field' => 'name',
        'col' => ['col-md-12'],
    ];

	protected function options(array $options)
	{
        $this->setAttributes([
            'options' => $options,
        ]);
	}
}
