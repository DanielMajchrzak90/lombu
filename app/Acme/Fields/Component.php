<?php

namespace Acme\Fields;

abstract class Component implements ComponentInterface
{
    protected $children = [];	
    protected $attributes = [];   

    public function __call($method, $parameters)
    {
        if (method_exists($this, $method)) {
            $this->$method(...$parameters); 
            return clone $this;               
        } else {
            throw new \Exception('Method '.$method.' does not exists');
        }
    }

    protected function setAttributes($attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    protected function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function getComponent()
    {
        return $this->component;
    }

    protected function setChildren($children)
    {
        $this->children = $children;
    }

    public function display()
    {   
        return [
            'name' => $this->component,
            'attributes' => $this->attributes,
            'children' => collect($this->children)->map(function($child) {
                return $child->display();
            }),
        ];
    }
}