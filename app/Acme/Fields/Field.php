<?php

namespace Acme\Fields;

class Field extends Component
{
    protected $component = 'text-field';
    protected $attributes = [
        'type' => 'text',
        'col' => ['col-md-12'],
    ];

	protected function make($name, $label = null)
	{
        $this->setAttributes([
            'name' => $name,
            'label' => $label,
        ]);
	}

    protected function help($help)
    {
        $this->setAttributes([
            'help' => $help,
        ]);
    }

    protected function col(array $col)
    {
        $this->setAttributes([
            'col' => $col,
        ]);
    }

	public function display()
	{	
        \Log::info(uniqid());
        $data = parent::display();
        $data['attributes']['id'] = letters_random(10);

     	return $data;
	}
}