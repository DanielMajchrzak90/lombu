<?php

namespace Acme\Fields;

class PasswordField extends Field
{
    protected $attributes = [
        'type' => 'password',
        'col' => ['col-md-12'],
    ];
}