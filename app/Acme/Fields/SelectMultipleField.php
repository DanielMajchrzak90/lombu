<?php

namespace Acme\Fields;

class SelectMultipleField extends Field
{
	protected $component = 'select-multiple-field';
	
    protected $attributes = [
		'value_field' => 'id',
		'label_field' => 'name',
        'col' => ['col-md-12'],
    ];

	protected function options(array $options)
	{
        $this->setAttributes([
            'options' => $options,
        ]);
	}

	protected function defaultValue($value)
	{
		$this->setAttribute('default_value', [$value]);
	}
}
