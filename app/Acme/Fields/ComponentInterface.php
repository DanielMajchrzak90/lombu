<?php

namespace Acme\Fields;

interface ComponentInterface
{
	public function display();
	public function getComponent();
}