<?php

namespace Acme\Fields;

class CodeEditorField extends Field
{
    protected $component = 'code-editor-field';
}