<?php

namespace Acme\Fields;

class TextField extends Field
{
	protected function mask($mask)
	{
        $this->setAttributes([
            'mask' => $mask,
        ]);
	}
}