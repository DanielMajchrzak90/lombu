<?php

namespace Acme\Fields;

class NestedComponent extends Field
{	
    protected $component = 'nested-component';
    protected $attributes = [
        'col' => ['col-md-12'],
    ];

	protected function make($resource, $label = null)
	{
        $this->setAttributes([
            'nested_resource' => $resource,
            'label' => $label,
        ]);
	}

    protected function fields(array $components)
    {
    	$this->setChildren($components);
    }
}