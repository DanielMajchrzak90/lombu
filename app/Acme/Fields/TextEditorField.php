<?php

namespace Acme\Fields;

class TextEditorField extends Field
{
    protected $component = 'text-editor-field';
}