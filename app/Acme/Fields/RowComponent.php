<?php

namespace Acme\Fields;

use Acme\Fields\Component;

class RowComponent extends Component
{
    protected $component = 'row-component';

    protected function make(array $components)
    {
    	$this->setChildren($components);
    }
}