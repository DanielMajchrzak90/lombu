<?php

namespace Acme\Fields;

class VueTextEditorField extends Field
{
    protected $component = 'vue-text-editor-field';
}