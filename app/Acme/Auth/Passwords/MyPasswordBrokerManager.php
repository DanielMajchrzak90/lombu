<?php

namespace Acme\Auth\Passwords;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Contracts\Auth\PasswordBrokerFactory as FactoryContract;
use Illuminate\Auth\Passwords\PasswordBrokerManager;

/**
 * @mixin \Illuminate\Contracts\Auth\PasswordBroker
 */
class MyPasswordBrokerManager extends PasswordBrokerManager
{
    protected function resolve($name)
    {
        $config = $this->getConfig($name);

        if (is_null($config)) {
            throw new InvalidArgumentException("Password resetter [{$name}] is not defined.");
        }

        // The password broker uses a token repository to validate tokens and send user
        // password e-mails, as well as validating that password reset process as an
        // aggregate service of sorts providing a convenient interface for resets.
        return new PasswordBroker(
            $this->createTokenRepository($config),
            $this->app['auth']->createUserProvider($config['provider'] ?: null)
        );
    }

    /**
     * @inheritdoc
     */
    protected function createTokenRepository(array $config)
    {
        $laravel = app();

        if (version_compare($laravel::VERSION, '5.4', '>=')) {
            return new DatabaseTokenRepository(
                $this->app['db']->connection(),
                $this->app['hash'],
                $config['table'],
                $this->app['config']['app.key'],
                $config['expire']
            );
        } else {
            return new DatabaseTokenRepository(
                $this->app['db']->connection(),
                $config['table'],
                $this->app['config']['app.key'],
                $config['expire']
            );
        }
    }
}
