<?php

namespace Acme\Auth;

use Illuminate\Auth\TokenGuard;
use Illuminate\Auth\Events\Logout;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Storage;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Signer\Keychain; 
use Cookie;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Auth\GuardHelpers;
use App\Exceptions\CustomException;
use GuzzleHttp\Exception\ClientException;

class JwtGuard implements Guard
{
    use GuardHelpers;

    protected $token = null;

    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    public function attempt(array $credentials = [], $remember = false)
    {
        $this->lastAttempted = $user = $this->provider->retrieveByCredentials($credentials);
        
        // If an implementation of UserInterface was returned, we'll ask the provider
        // to validate the user against the given credentials, and if they are in
        // fact valid we'll log the users into the application and return true.
        if ($this->hasValidCredentials($user, $credentials)) {
            $this->login($user, $remember);

            return true;
        }

        return false;
    }

    public function login(AuthenticatableContract $user, $remember = false)
    {
        $disk = Storage::disk('openssl_key_pair');
        $signer = new Sha256();

        $privateKey = new Key($disk->get('private.key'));

        $token = (new Builder())
            ->setIssuer("user:{$user->_id}")
            ->setIssuedAt(time())
            ->setExpiration($this->getExpiresAt($remember)->timestamp)                  
            ->set('id', $user->id)
            ->sign($signer, $privateKey)
            ->getToken();

        $this->token = $token;

        \Cookie::queue(\Cookie::forever('jwt_token', (string) $token));
    }

    public function user()
    {
        //return;
        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (!is_null($this->user)) {
            return $this->user;
        }

        $authorization = request()->header('auth');
        $token = null;

        if (!$authorization) {
            $token = request()->cookie('jwt_token');
        }

        if (preg_match('/Bearer\s(\S+)/', $authorization, $matches)) {
            $token = $matches[1];
        }

        try {
            $token = (new Parser())->parse($token);
        } catch (\Exception $e) {
            $token = null;
        }

        if (!$token)
            return null;

        $signer = new Sha256();
        $keychain = new Keychain();
        $disk = Storage::disk('openssl_key_pair');
        $pubKeyUrl = 'file://' . storage_path('app/openssl_key_pair/public.key');
        $pubKey = $keychain->getPublicKey($pubKeyUrl);

        $verified = $token->verify($signer, $keychain->getPublicKey($pubKeyUrl));

        $exp = $token->getClaim('exp');
        $now = Carbon::now()->timestamp;

        if ($now > $exp)
            return null;

        $id = $token->getClaim('id');
        $this->user = User::find($id);

        return $this->user;
    }  

    protected function hasValidCredentials($user, $credentials)
    {
        return ! is_null($user) && $this->provider->validateCredentials($user, $credentials);
    }

    /**
     * Validate a user's credentials.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        if (empty($credentials[$this->inputKey])) {
            return false;
        }

        $credentials = [$this->storageKey => $credentials[$this->inputKey]];

        if ($this->provider->retrieveByCredentials($credentials)) {
            return true;
        }

        return false;
    }

    public function getToken()
    {
        return (string) $this->token;
    }

    public function getExpiresAt($remember = false)
    {
        return (!$remember) ? Carbon::now()->addMinutes(120) : Carbon::now()->addDays(30);
    }
    
    public function logout()
    {
        \Cookie::queue(\Cookie::forget('jwt_token'));
    }
}