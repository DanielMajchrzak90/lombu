<?php

namespace Acme\Observers;

use App\Models\File;
use Storage;

class FileObserver
{
	public function deleting(File $file)
    {
        $disk = Storage::disk('ck_files');

        if ($disk->exists($file->filename)) {
        	$disk->delete($file->filename);
        }
    }
}