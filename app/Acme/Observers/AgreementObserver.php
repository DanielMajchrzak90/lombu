<?php

namespace Acme\Observers;

use App\Models\Agreement;
use App\Models\Customer;

class AgreementObserver
{
	public function created(Agreement $agreement)
	{
		$agreement->setCustomer();
	}

	public function updated(Agreement $agreement)
	{
		$agreement->setCustomer();
	}
}