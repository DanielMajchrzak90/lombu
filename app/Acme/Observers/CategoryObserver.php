<?php

namespace Acme\Observers;

use App\Models\Category;
use DB;

class CategoryObserver
{
    public function creating(Category $item)
    {
        Category::resetOrder($item->parent_id);
        $whereClause = ($item->parent_id) ? 
            'where parent_id = '.$item->parent_id :
            'where parent_id is null';

        $query = '(SELECT COUNT(id) FROM '.Category::getTableName().' as myorder '.$whereClause.')';
        $item->order = DB::raw($query);
    }

    public function created(Category $item)
    {

    }

    public function updating(Category $item)
    {        
        Category::resetOrder($item->parent_id);
    }

    public function updated(Category $item)
    {
        $old = $item->getOriginal();

        if ($item->order != $old['order'] && 
            $item->parent_id == $old['parent_id']) {
            if ($item->order > $old['order']) {
                Category::UpdateDownOrder(
                    $item->order, 
                    $old['order'], 
                    $item->id,
                    $item->parent_id
                );               
            } elseif ($item->order < $old['order']) {
                Category::UpdateUpOrder(
                    $item->order, 
                    $old['order'], 
                    $item->id,
                    $item->parent_id
                );                 
            }
        }

        elseif ($item->parent_id != $old['parent_id']) { 
            Category::changeParentOrder(
                $item->order, 
                $item->id,
                $item->parent_id
            );       
        }
    }

    public function deleted(Category $item)
    {
        Category::resetOrder($item->parent_id);
    }

    public function restored(User $user)
    {
        //
    }

    public function forceDeleted(User $user)
    {
        //
    }
}
