<?php

namespace Acme\Observers;

use App\Models\Lombard;
use App\Models\AgreementPattern;

class LombardObserver
{
	public function created(Lombard $lombard)
	{
		factory(AgreementPattern::class, 1)->create([
			'lombard_id' => $lombard->id,	
			'name' => __('messages.default')
		]);
	}
}