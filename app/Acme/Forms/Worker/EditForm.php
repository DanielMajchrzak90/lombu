<?php

namespace Acme\Forms\Worker;

use Acme\Fields\TextField;
use Acme\Fields\PasswordField;
use Acme\Fields\SelectMultipleField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class EditForm extends BaseForm
{
	public function fields(
		TextField $text, 
		PasswordField $password, 
		SelectMultipleField $select, 
		RowComponent $row
	)
	{
        $user = $this->request->user();
        $lombards = $user->lombards;

		return [
			$row->make([
				$select->make('lombards',  __('messages.lombards'))->options(
					$lombards->toArray()
				),
				$text->make('name',  __('messages.name')),
				$text->make('email',  __('messages.email')),
			]),
		];
	}
}