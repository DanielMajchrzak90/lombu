<?php

namespace Acme\Forms\Worker;

use Acme\Fields\TextField;
use Acme\Fields\PasswordField;
use Acme\Fields\SelectMultipleField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class CreateForm extends BaseForm
{
	public function fields(
		TextField $text, 
		PasswordField $password, 
		SelectMultipleField $select, 
		RowComponent $row
	)
	{
        $user = $this->request->user();
        $lombards = $user->lombards;

		return [
			$row->make([
				$select->make('lombards',  __('messages.lombards'))->options(
					$lombards->toArray()
				)->defaultValue($lombards->first() ? $lombards->first()->id : null),
				$text->make('name',  __('messages.name')),
				$text->make('email',  __('messages.email')),
				$password->make('password',  __('messages.password'))
					->help('Password must have number, big letter, special char and min. 8 chars.'),
			]),
		];
	}
}