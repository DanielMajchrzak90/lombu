<?php

namespace Acme\Forms\Lombard;

use Acme\Fields\TextField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form;

class EditForm extends Form
{
	public function fields(
		TextField $text, 
		RowComponent $row
	)
	{
		return [
			$row->make([
				$text->make('name',  __('messages.name'))->col(['col-md-12', 'col-lg-6']),
				$text->make('street',  __('messages.street'))->col(['col-md-12', 'col-lg-6']),
			]),
			$row->make([
				$text->make('street_no',  __('messages.street_no'))->col(['col-md-12', 'col-lg-6']),
				$text->make('flat_no',  __('messages.flat_no'))->col(['col-md-12', 'col-lg-6']),
			]),
			$row->make([
				$text->make('postal_code',  __('messages.postal_code'))
					->mask('99-999')
					->col(['col-md-3']),
				$text->make('city',  __('messages.city'))->col(['col-md-9']),
			]),
		];
	}
}