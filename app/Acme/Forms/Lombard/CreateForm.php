<?php

namespace Acme\Forms\Lombard;

use Acme\Fields\TextField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form;

class CreateForm extends Form
{
	public function fields(TextField $text, RowComponent $row)
	{
		return [
			$row->make([
				$text->make('name',  __('messages.name'))->col(['col-md-12']),
			]),
		];
	}
}