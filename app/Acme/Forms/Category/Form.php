<?php

namespace Acme\Forms\Category;

use Acme\Fields\TextField;
use Acme\Fields\TextEditorField;
use Acme\Fields\CodeEditorField;
use Acme\Fields\ToggleField;
use Acme\Fields\SelectField;
use Acme\Fields\DatepickerField;
use Acme\Fields\NestedField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class Form extends BaseForm
{
	public function fields(
		TextField $text, 
		SelectField $select,
		RowComponent $row
	)
	{
		$contentHelp = view('forms.agreement_pattern.help.content')->render();
        $lombard = $this->request->route('lombard');

		return [
			$row->make([
				$select->make('parent_id',  __('messages.parent'))->options(
					$lombard->categories->toArray()
				),
			]),
			$row->make([
				$text->make('name',  __('messages.name')),
			]),
		];
	}
}