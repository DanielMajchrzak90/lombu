<?php

namespace Acme\Forms\AgreementRenewal;

use Acme\Fields\DatepickerField;
use Acme\Fields\TextField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class Form extends BaseForm
{
	public function fields(
		TextField $text, 
		DatepickerField $datepicker,
		RowComponent $row
	)
	{
		$contentHelp = view('forms.agreement_pattern.help.content')->render();

		return [
			$row->make([
				$datepicker->make('end_at',  __('messages.end_at'))
					->col(['col-md-6'])
					->setAttribute('calendar_class', 'datepicker-position-bottom'),
				$text->make('amount',  __('messages.amount'))->col(['col-md-6']),
			]),
		];
	}
}