<?php

namespace Acme\Forms\AgreementPattern;

use Acme\Fields\TextField;
use Acme\Fields\TextEditorField;
use Acme\Fields\VueTextEditorField;
use Acme\Fields\CodeEditorField;
use Acme\Fields\ToggleField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class Form extends BaseForm
{
	public function fields(
		TextField $text, 
		TextEditorField $textEditor,
		CodeEditorField $codeEditor,
		ToggleField $toggle,
		RowComponent $row
	)
	{
		$contentHelp = view('forms.agreement_pattern.help.content')->render();

		return [
			$row->make([
				$text->make('name',  __('messages.name')),
				$toggle->make('content',  __('messages.content'))->help($contentHelp)->add( 
					$textEditor->setAttributes([
						'name' => __('messages.article_editor')
					])
				)->add(
					$codeEditor->setAttributes([
						'name' => __('messages.code_editor')
					])
				)
			]),
		];
	}
}