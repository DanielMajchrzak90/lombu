<?php

namespace Acme\Forms\CkFiles;

use Acme\Fields\FilesField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class Form extends BaseForm
{
	public function fields(
		FilesField $files, 
		RowComponent $row
	)
	{
		return [
			$row->make([
				$files->make('ck_files',  __('messages.files'))
					->placeholder(__('messages.choose_files')),
			]),
		];
	}
}