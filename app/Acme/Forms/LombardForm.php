<?php

namespace Acme\Forms;

use Acme\Fields\TextField;

class LombardForm extends Form
{
	public function fields(TextField $text)
	{
		return [
			$text->make('name',  __('messages.name')),
			$text->make('street',  __('messages.street')),
			$text->make('street_no',  __('messages.street_no')),
			$text->make('flat_no',  __('messages.flat_no')),
			$text->make('postal_code',  __('messages.postal_code')),
			$text->make('city',  __('messages.city')),
		];
	}
}