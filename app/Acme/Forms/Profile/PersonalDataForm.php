<?php

namespace Acme\Forms\Profile;

use Acme\Fields\TextField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class PersonalDataForm extends BaseForm
{
	public function fields(
		TextField $text, 
		RowComponent $row
	)
	{
		return [
			$row->make([
				$text->make('name',  __('messages.name')),
				$text->make('email',  __('messages.email')),
			]),
		];
	}
}