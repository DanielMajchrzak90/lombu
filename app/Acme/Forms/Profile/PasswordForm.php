<?php

namespace Acme\Forms\Profile;

use Acme\Fields\PasswordField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class PasswordForm extends BaseForm
{
	public function fields(
		PasswordField $text, 
		RowComponent $row
	)
	{
		return [
			$row->make([
				$text->make('current_password',  __('messages.current_password')),
				$text->make('new_password',  __('messages.new_password'))
					->help('Password must have number, big letter, special char and min. 8 chars.'),
			]),
		];
	}
}