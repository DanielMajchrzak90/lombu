<?php

namespace Acme\Forms\Auth;

use Acme\Fields\RowComponent;
use Acme\Fields\TextField;
use Acme\Fields\PasswordField;
use Illuminate\Routing\RouteDependencyResolverTrait;
use Illuminate\Container\Container;
use Acme\Forms\Form;

class LoginForm extends Form
{
	public function fields(
		TextField $text,
		PasswordField $password,
		RowComponent $row
	)
	{
		return [
			$row->make([
				$text->make('email',  __('messages.email')),
				$password->make('password',  __('messages.password')),
			])
		];
	}
}