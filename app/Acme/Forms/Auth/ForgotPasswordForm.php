<?php

namespace Acme\Forms\Auth;

use Acme\Fields\RowComponent;
use Acme\Fields\TextField;
use Acme\Fields\PasswordField;
use Illuminate\Routing\RouteDependencyResolverTrait;
use Illuminate\Container\Container;
use Acme\Forms\Form;

class ForgotPasswordForm extends Form
{
	public function fields(
		TextField $text,
		RowComponent $row
	)
	{
		return [
			$row->make([
				$text->make('email',  __('messages.email')),
			])
		];
	}
}