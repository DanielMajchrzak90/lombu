<?php

namespace Acme\Forms\Product;

use Acme\Fields\TextField;
use Acme\Fields\TextEditorField;
use Acme\Fields\CodeEditorField;
use Acme\Fields\ToggleField;
use Acme\Fields\SelectField;
use Acme\Fields\DatepickerField;
use Acme\Fields\NestedField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;

class Form extends BaseForm
{
	public function fields(
		TextField $text, 
		TextEditorField $textEditor,
		SelectField $select,
		RowComponent $row
	)
	{
        $lombard = $this->request->route('lombard');

		return [
			$row->make([
				$select->make('category_id',  __('messages.category_name'))->options(
					$lombard->categories->toArray()
				)->col(['col-md-6']),
				$text->make('name',  __('messages.name'))->col(['col-md-6']),
			]),
			$row->make([
				$text->make('market_price',  __('messages.market_price'))->col(['col-md-4']),
				$text->make('purchase_price',  __('messages.purchase_price'))->col(['col-md-4']),
				$text->make('selling_price',  __('messages.selling_price'))->col(['col-md-4']),
			]),
			$row->make([
				$textEditor->make('description',  __('messages.description')),
			]),
		];
	}
}