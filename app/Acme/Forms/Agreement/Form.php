<?php

namespace Acme\Forms\Agreement;

use Acme\Fields\TextField;
use Acme\Fields\TextEditorField;
use Acme\Fields\CodeEditorField;
use Acme\Fields\ToggleField;
use Acme\Fields\SelectField;
use Acme\Fields\DatepickerField;
use Acme\Fields\NestedComponent;
use Acme\Fields\AutoCompleteField;
use Acme\Fields\RowComponent;
use Acme\Forms\Form as BaseForm;
use App\Models\Product;

class Form extends BaseForm
{
	public function fields(
		TextField $text, 
		TextEditorField $textEditor,
		CodeEditorField $codeEditor,
		ToggleField $toggle,
		SelectField $select,
		DatepickerField $datepicker,
		AutoCompleteField $autocomplete,
		NestedComponent $nested,
		RowComponent $row
	)
	{
        $lombard = $this->request->route('lombard');
        $customers = $lombard->customers;

		return [
			$row->make([
				$select->make('agreement_pattern_id',  __('messages.agreement_pattern'))->options(
					$lombard->agreementsPatterns->toArray()
				)->col(['col-md-6']),
			]),
			$row->make([
				$autocomplete->make('pesel',  __('messages.pesel'))
					->mask('999999999')
					->col(['col-md-6'])
					->options($customers->toArray())
					->setAttributes([
						'value_field' => 'pesel',
						'label_field' => 'full_name',
						'emit_name' => 'pesel-selected',
					]),
				$text->make('id_number_and_series',  __('messages.id_number_and_series'))
					->mask('aaa999')
					->col(['col-md-6']),
			]),
			$row->make([
				$text->make('first_name',  __('messages.first_name'))->col(['col-md-6']),
				$text->make('last_name',  __('messages.last_name'))->col(['col-md-6']),
				$text->make('email',  __('messages.email'))->col(['col-md-6']),
			]),
			$row->make([
				$datepicker->make('end_at',  __('messages.end_at'))->col(['col-md-4']),
				$text->make('amount',  __('messages.amount'))->col(['col-md-4']),
				$text->make('cost_per_day',  __('messages.cost_per_day'))->col(['col-md-4']),
			]),
			$row->make([
				$nested->make('products', __('messages.products'))->fields([
					$row->make([
						$text->make('name',  __('messages.name'))->col(['col-md-6']),
						$text->make('market_price',  __('messages.market_price'))->col(['col-md-6'])
					]),
				])
			])
		];
	}
}