<?php

namespace Acme\Forms;

use Illuminate\Container\Container;
use Illuminate\Http\Request;

class Form
{
    protected $container;
    protected $request;

    public function __construct(Container $container, Request $request)
    {
        $this->container = $container;
        $this->request = $request;
    }

	protected function collectionFields()
	{
		$fields = $this->resolveFieldsDependecies();

		return collect($fields);
	}

	public function formFields()
	{
		$fields = $this->collectionFields()->map(function($field) {
			return $field->display();
		});

		return $fields;
	}

	protected function resolveFieldsDependecies()
	{
		$reflection = new \ReflectionMethod($this, 'fields');
		$params = $reflection->getParameters();
		$params = collect($params)->map(function($param) {
			return $this->transformDependency($param);
		})->toArray();


        return $this->fields(...array_values($params));
	}

	protected function transformDependency(\ReflectionParameter $parameter)
    {
        $class = $parameter->getClass();

        if ($class) {
            return $this->container->make($class->name);
        }
    }
}