<?php

namespace Acme\Eloquent;

trait TreeTrait
{    
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }
}