<?php

namespace Acme\Eloquent;

use Closure;
use BadMethodCallException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Pagination\Paginator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Concerns\BuildsQueries;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Acme\EloquentTree\Helpers\QueriesRelationships;

class Builder extends \Illuminate\Database\Eloquent\Builder
{	
	protected $items = [];

	public function tree()
	{
		$items = $this->get();
		$items = $items->groupBy('parent_id');
		$items = $items->map(function($item) use (&$items) {
			return $item->map(function($item) use (&$items) {
				return $this->getChildren($item, $items);
			});
		})->first();

		return $items;
	}

	protected function getChildren($parent, &$items)
	{
		if (isset($items[$parent->id])) {
			$children = $items->pull($parent->id);
			$children = $children->map(function($item) use (&$items) {
				return $this->getChildren($item, $items);
			});			
			$parent->setRelation('children', $children);	
		} 

		return $parent;
	}
}