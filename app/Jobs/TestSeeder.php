<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\User;
use App\Models\Lombard;
use App\Models\Agreement;
use App\Models\Product;
use App\Models\Category;
use App\Models\AgreementPattern;
use App\Models\AgreementRenewal;

class TestSeeder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $lombard = factory(Lombard::class)
            ->create([
                'user_id' => $user->id,
            ]);

        factory(Category::class, 5)
            ->create([
                'lombard_id' => $lombard->id,
            ])
            ->each(function ($c, $index) use ($lombard) {                   
                factory(Category::class, 3)
                    ->create([
                        'lombard_id' => $lombard->id,
                        'parent_id' => $c->id,
                    ])
                    ->each(function ($c, $index) use ($lombard) {
                        factory(Category::class, 2)
                        ->create([
                            'lombard_id' => $lombard->id,
                            'parent_id' => $c->id,
                        ]);
                    });
            });

        $agreementsPattern = 
            $lombard->agreementsPatterns->first() ?: 
            factory(AgreementPattern::class)->create([
                'lombard_id' => $lombard->id,   
            ]);

        factory(Agreement::class, 20)
            ->create([
                'lombard_id' => $lombard->id,   
                'agreement_pattern_id' => $agreementsPattern->id,
            ])
            ->each(function ($agreement, $index) use ($lombard) {                   
                factory(Product::class, 2)
                    ->create([
                        'lombard_id' => $lombard->id,
                        'agreement_id' => $agreement->id,
                    ]);
                 
                factory(AgreementRenewal::class, 2)
                    ->create([
                        'agreement_id' => $agreement->id, 
                    ]);
            });

        for ($i=0; $i < 30; $i++) { 
            $category = $lombard->categories->shuffle()->first();
            factory(Product::class)
                ->create([
                    'agreement_id' => null, 
                    'lombard_id' => $lombard->id, 
                    'order' => null,  
                    'category_id' => $category->id
                ]);  
        }

        factory(User::class, 2)
            ->create([
                'employer_id' => $user->id,
            ]);
    }
}
