<?php

if (!function_exists('letters_random')) {
    function letters_random($number = 5) {
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $rand = '';

        foreach (array_rand($seed, $number) as $k) $rand .= $seed[$k];

        return $rand;
    }
}