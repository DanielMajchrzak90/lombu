<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Lombard;
use Illuminate\Auth\Access\HandlesAuthorization;

class LombardPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user, Lombard $item)
    {
        return true;
    }

    public function create(User $user, Lombard $item)
    {
        return $user->is_employer;
    }

    public function update(User $user, Lombard $item)
    {
        return $user->is_employer;
    }

    public function delete(User $user, Lombard $item)
    {
        return $user->is_employer;
    }
}
