<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Agreement;
use Illuminate\Auth\Access\HandlesAuthorization;

class AgreementPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user, Agreement $item)
    {
        return !!$user->agreements()->find($item->id);
    }
}
