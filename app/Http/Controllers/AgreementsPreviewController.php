<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dompdf\Dompdf;

class AgreementsPreviewController extends Controller
{
    public function show(Request $request)
    {
    	$user = $request->user();
    	$agreement = $request->route('agreement_preview');
    	
    	if (!$user->can('view', $agreement)) {
    		abort(403, __('messages.forbidden'));
		}

    	$content = $agreement->content;
    	
		$pdf = \PDF::loadView('agreements.preview', compact('content'));

    	return $pdf->stream();    	
    }
}
