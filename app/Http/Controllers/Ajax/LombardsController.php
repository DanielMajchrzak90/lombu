<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use Acme\Transformers\Ajax\Lombard\IndexTransformer;
use Acme\Transformers\Ajax\Lombard\ShowTransformer;
use Acme\Transformers\Ajax\Lombard\EditTransformer;
use App\Models\Lombard;
use App\Http\Requests\Ajax\Lombard\StoreRequest;
use App\Http\Requests\Ajax\Lombard\UpdateRequest;
use Auth;
use Acme\Forms\Lombard\CreateForm;
use Acme\Forms\Lombard\EditForm;
use DB;

class LombardsController extends Controller
{
    protected $data = [
        'name', 
        'street', 
        'street_no', 
        'flat_no',
        'postal_code',
        'city', 
    ];

    protected $forms = [];

    public function __construct(
        CreateForm $createForm,
        EditForm $editForm
    )
    {
        $this->forms = [
            'create' => $createForm,
            'edit' => $editForm,
        ]; 
    }

    public function index(IndexTransformer $transformer) 
    {
        $user = Auth::user(); 
        $lombards = $user->lombards()
            ->orderBy('created_at', 'desc')
            ->paginate();    
            
        $lombards->getCollection()
        	->transform(function($lombard) use ($transformer) {
	            return $transformer->transform($lombard);
	        });

        return $this->response([
            'items' => $lombards,
        ]);
    }

    public function store(StoreRequest $request)
    {
        DB::transaction(function() use ($request) {
            $data = $request->only('name');
            $user = Auth::user();

            $user->lombards()->create($data);
        });

        return $this->responseSuccess('actions.lombard_created');
    }

    public function show(Request $request, ShowTransformer $transformer)
    {
        $lombard = $request->route('lombard');

        return $this->response([
            'item' => $transformer->transform($lombard),
        ]);
    }

    public function edit(Request $request, EditTransformer $transformer)
    {
        $lombard = $request->route('lombard');

        return $this->response([
            'item' => $transformer->transform($lombard),
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->only($this->data);

        $lombard = $request->route('lombard');
        $lombard->update($data);

        return $this->responseSuccess('actions.lombard_updated');
    }

    public function destroy(Request $request)
    {
        $lombard = $request->route('lombard');
        $lombard->delete();

        return $this->responseSuccess('actions.lombard_deleted');
    }

    public function fields(Request $request)
    {
        $form = $this->forms[$request->route('type')];
        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
