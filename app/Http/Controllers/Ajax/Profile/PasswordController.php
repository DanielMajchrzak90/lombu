<?php

namespace App\Http\Controllers\Ajax\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Profile\Password\UpdateRequest;
use Acme\Forms\Profile\PasswordForm;

class PasswordController extends Controller
{
    public function update(UpdateRequest $request)
    {
    	$user = $request->user();
    	$user->update([
    		'password' => $request->new_password,
    	]);

    	return $this->responseSuccess('actions.password_updated');
    }

    public function fields(Request $request, PasswordForm $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
