<?php

namespace App\Http\Controllers\Ajax\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Profile\PersonalData\UpdateRequest;
use Acme\Forms\Profile\PersonalDataForm;

class PersonalDataController extends Controller
{
    public function update(UpdateRequest $request)
    {
    	$user = $request->user();
    	$user->update($request->only([
    		'name', 'email',
    	]));

        return $this->responseSuccess('actions.profile_updated');
    }

    public function edit(Request $request)
    {
        $user = \Auth::user();

        return response()->json([
            'item' => $user->only(['name', 'email'])
        ]);
    }

    public function fields(Request $request, PersonalDataForm $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
