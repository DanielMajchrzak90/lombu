<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
	protected function responseErrorAuthorization($msg)
	{
		return $this->responseError($msg, 401);
	}

    protected function responseError($msg, $code = 400)
    {
    	return $this->response([
            'error' => trans($msg)
        ], $code);
    }

    protected function responseSuccess($msg, $attributes = [])
    {
    	$response = array_merge([
            'msg' => trans($msg)
        ], $attributes);

    	return $this->response($response);
    }

    protected function response($response, $code = 200) 
    {
    	return response()->json($response, $code);
    }
}
