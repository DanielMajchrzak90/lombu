<?php

namespace App\Http\Controllers\Ajax\Lombards;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Lombard\AgreementPattern\StoreRequest;
use App\Http\Requests\Ajax\Lombard\AgreementPattern\UpdateRequest;
use App\Models\User;
use Acme\Transformers\Ajax\Lombard\Show\AgreementPattern\IndexTransformer;
use Acme\Transformers\Ajax\Lombard\Show\AgreementPattern\EditTransformer;
use Acme\Forms\AgreementPattern\Form;

class AgreementsPatternsController extends Controller
{
	protected $data = [
		'name', 'content'
	];

    public function index(Request $request, IndexTransformer $transformer)
    {        
        $lombard = $request->route('lombard');
        $agreementsPatterns = $lombard->agreementsPatterns()
        	->get()
            ->map(function($agreementPattern) use ($transformer) {
                return $transformer->transform($agreementPattern);
            });

        return $this->response([
            'items' => $agreementsPatterns,
        ]);
    }

    public function store(StoreRequest $request)
    {
        $data = $request->only($this->data);

    	$lombard = $request->route('lombard');
    	$lombard->agreementsPatterns()
    		->create($data);

        return $this->responseSuccess('actions.agreement_pattern_created');
    }

    public function edit(Request $request, EditTransformer $transformer)
    {
        $agreementPattern = $request->route('agreement_pattern');

        return $this->response([
            'item' => $transformer->transform($agreementPattern),
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->only($this->data);

        $agreementPattern = $request->route('agreement_pattern');
        $agreementPattern->update($data);

        return $this->responseSuccess('actions.agreement_pattern_updated');
    }

    public function destroy(Request $request)
    {
        $agreementPattern = $request->route('agreement_pattern');
        $agreementPattern->delete();

        return $this->responseSuccess('actions.agreement_pattern_deleted');
    }

    public function fields(Request $request, Form $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
