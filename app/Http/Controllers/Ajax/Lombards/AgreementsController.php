<?php

namespace App\Http\Controllers\Ajax\Lombards;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Lombard\Agreement\StoreRequest;
use App\Http\Requests\Ajax\Lombard\Agreement\UpdateRequest;
use App\Models\Lombard\Product;
use Acme\Transformers\Ajax\Lombard\Show\Agreement\IndexTransformer;
use Acme\Transformers\Ajax\Lombard\Show\Agreement\EditTransformer;
use DB;
use Acme\Forms\Agreement\Form;

class AgreementsController extends Controller
{
	protected $attributes = [
		'first_name', 
		'last_name', 
		'pesel', 
		'email', 
		'id_number_and_series', 
		'end_at',
        'amount',
        'cost_per_day',
        'agreement_pattern_id',
	];

    public function index(Request $request, IndexTransformer $transformer)
    {        
        $lombard = $request->route('lombard');
        $agreements = $lombard->agreements()->search(
            $request->search_phrase, 
            $request->sort_col,
            $request->sort_type
        )->paginate();

        $agreements->getCollection()
            ->transform(function($agreement) use ($transformer) {
                return $transformer->transform($agreement);
            });

        return $this->response([
            'items' => $agreements,
        ]);
    }

    public function store(StoreRequest $request)
    {
        return DB::transaction(function () use ($request) {
        	$lombard = $request->route('lombard');
            $data = $request->only($this->attributes);
        	$agreement = $lombard->agreements()->create($data);
            $agreement->setProducts($request->products);

            return $this->responseSuccess('actions.agreement_created', [
                'agreement_id' => $agreement->id,
            ]);
        });
    }

    public function edit(Request $request, EditTransformer $transformer)
    {    	
        $agreement = $request->route('agreement');
        $agreement = $transformer->transform($agreement);

        return $this->response([
            'item' => $agreement,
        ]);
    }

    public function update(StoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $agreement = $request->route('agreement');
            $data = $request->only($this->attributes);
            $agreement->update($data);
            $agreement->setProducts($request->products);
        });

        return $this->responseSuccess('actions.agreement_updated');
    }

    public function destroy(Request $request)
    {
        $agreement = $request->route('agreement');
        $agreement->delete();
        
        return $this->responseSuccess('actions.agreement_deleted');
    }

    public function fields(Request $request, Form $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
