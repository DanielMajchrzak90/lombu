<?php

namespace App\Http\Controllers\Ajax\Lombards;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Lombard\Product\StoreRequest;
use App\Http\Requests\Ajax\Lombard\Product\UpdateRequest;
use App\Models\Product;
use DB;
use Acme\Forms\Product\Form;

class ProductsController extends Controller
{
	protected $productAttributes = [
        'name',
        'description',
        'market_price',
        'purchase_price',
        'selling_price',
        'created_at',
        'category_id',
	];

    public function index(Request $request)
    {        
        $lombard = $request->route('lombard');
        $products = $lombard->products()
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->select('products.*', 'categories.name as category_name')
            ->search(
                $request->search_phrase, 
                $request->sort_col,
                $request->sort_type
            )->paginate();

        $products->getCollection()
            ->transform(function($product) {
                return $product->only([
                    'name',
                    'category_name',
                    'description',
                    'market_price' => 'market_price_formatted',
                    'purchase_price' => 'purchase_price_formatted',
                    'selling_price' => 'selling_price_formatted',
                    'created_at',
                ]);
            });

        return $this->response([
            'items' => $products,
        ]);
    }

    public function store(StoreRequest $request)
    {
        return DB::transaction(function () use ($request) {
        	$lombard = $request->route('lombard');
            $product = $request->only($this->productAttributes);
        	$product = $lombard->products()
        		->create($product);

            return $this->responseSuccess('actions.product_created');
        });
    }

    public function edit(Request $request)
    {    	
        $product = $request->route('product');
        $product = $product->only($this->productAttributes);

        return $this->response([
            'item' => $product,
        ]);
    }

    public function update(StoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $product = $request->route('product');
            $product->update($request->only($this->productAttributes));
        });

        return $this->responseSuccess('actions.product_updated');
    	
    }

    public function destroy(Request $request)
    {
        $product = $request->route('product');
        $product->delete();
        
        return $this->responseSuccess('actions.product_deleted');
    }

    public function fields(Request $request, Form $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
