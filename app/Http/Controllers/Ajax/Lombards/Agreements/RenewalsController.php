<?php

namespace App\Http\Controllers\Ajax\Lombards\Agreements;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use Acme\Forms\AgreementRenewal\Form;
use DB;
use App\Http\Requests\Ajax\Lombard\Agreement\Renewal\StoreRequest;

class RenewalsController extends Controller
{
    public function index(Request $request)
    {
    	$agreement = $request->route('agreement');
    	$renewals = $agreement->renewals->map(function($renewal) {
            return $renewal->only([
                'amount', 'end_at', 'created_at',
            ]);
        });
    	
        return $this->response([
            'items' => $renewals,
        ]);
    }

    public function store(StoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $agreement = $request->route('agreement');
            $data = $request->only(['amount', 'end_at']);
            $agreement->renewals()->create($data);
        });

        return $this->responseSuccess('actions.renewal_created');
    }

    public function update(StoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $renewal = $request->route('renewal');
            $data = $request->only(['amount', 'end_at']);
            $renewal->update($data);
        });

        return $this->responseSuccess('actions.renewal_updated');
    }

    public function destroy(Request $request)
    {
        $renewal = $request->route('renewal');
        $renewal->delete();
        
        return $this->responseSuccess('actions.renewal_deleted');
    }

    public function fields(Request $request, Form $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
