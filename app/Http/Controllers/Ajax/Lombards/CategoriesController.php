<?php

namespace App\Http\Controllers\Ajax\Lombards;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Lombard\Category\StoreRequest;
use App\Http\Requests\Ajax\Lombard\Category\UpdateRequest;
use App\Http\Requests\Ajax\Lombard\Category\UpdateNestableRequest;
use DB;
use App\Models\Category;
use Acme\Transformers\Ajax\Lombard\Show\Category\EditTransformer;
use Acme\Transformers\Ajax\Lombard\Show\Category\IndexTransformer;
use Acme\Forms\Category\Form;

class CategoriesController extends Controller
{
    public function index(Request $request, IndexTransformer $transformer)
    {    	
        $lombard = $request->route('lombard');        
        $categories = $lombard->categories()->search(
            $request->search_phrase, 
            $request->sort_col,
            $request->sort_type
        )->paginate();
        
        $categories->getCollection()
            ->transform(function($category) {
                return $category->only(['name']);
            });


        return $this->response([
            'items' => $categories,
        ]);
    }
 
    public function tree(Request $request)
    {       
        $lombard = $request->route('lombard');        
        $tree = $lombard->categories()->tree();

        return $this->response([
            'tree' => $tree,
        ]);
    }

    public function store(StoreRequest $request)
    {
        $lombard = $request->route('lombard');
        $data = $request->only('name', 'parent_id');
        $category = $lombard->categories()->create($data);

        return $this->responseSuccess('actions.category_created');
    }

    public function edit(Request $request, EditTransformer $transformer)
    {
        $category = $request->route('category');

        return $this->response([
            'item' => $transformer->transform($category),
        ]);
    }

    public function update(UpdateRequest $request)
    {
        DB::transaction(function () use ($request) {
            $category = $request->route('category');
            $data = $request->only('name', 'parent_id');
            $category->update($data);
        });

        return $this->responseSuccess('actions.category_updated');
    }

    public function updateNestable(UpdateNestableRequest $request)
    {
        DB::transaction(function () use ($request) {
            $category = $request->route('category');
            $data = $request->only('order', 'parent_id'); 
            
            $category->update($data);
        });

        return $this->responseSuccess('actions.category_updated');
    }

    public function destroy(Request $request)
    {
        $category = $request->route('category');
        $category->delete();

        return $this->responseSuccess('actions.category_deleted');
    }

    public function fields(Request $request, Form $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
