<?php

namespace App\Http\Controllers\Ajax\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Auth\PasswordExpiredResetRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetExpiredPasswordController extends BaseResetPasswordController
{
    public function reset(PasswordExpiredResetRequest $request)
    {
        return parent::baseReset($request);
    }
}
