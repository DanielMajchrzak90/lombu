<?php

namespace App\Http\Controllers\Ajax\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use Acme\Forms\Auth\LoginForm;
use Acme\Forms\Auth\RegisterForm;
use Acme\Forms\Auth\ForgotPasswordForm;
use Acme\Forms\Auth\ResetPasswordForm;

class FormFieldsController extends Controller
{
	protected $forms = [];

	public function __construct(
		LoginForm $loginForm,
		RegisterForm $registerForm,
		ForgotPasswordForm $forgotPasswordForm,
		ResetPasswordForm $resetPasswordForm
	)
	{
		$this->forms = [
			'login' => $loginForm,
			'register' => $registerForm,
			'forgot_password' => $forgotPasswordForm,
			'reset_password' => $resetPasswordForm,
		]; 
	}

    public function fields(Request $request)
    {
    	$form = $this->forms[$request->route('type')];
    	
    	return $this->response([
            'fields' => $form->formFields(),
    	]);
    }
}
