<?php

namespace App\Http\Controllers\Ajax\Auth;

class ExpiredPasswordController extends RecoveryPasswordController
{
    protected function send($attributes)
    {
        return $this->broker()->sendExpiredResetLink($attributes);
    }
}
