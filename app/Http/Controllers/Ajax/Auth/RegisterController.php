<?php

namespace App\Http\Controllers\Ajax\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Auth\RegisterRequest;
use App\Models\User;
use Auth;
use App\Jobs\TestSeeder;
use DB;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request)
    {    	
    	$user = DB::transaction(function() use ($request) {
			$user = User::create([
	            'email' => $request->email,
	            'password' => $request->password,
	            'confirmed' => false,
	        ]);

			if (config('settings.test_seeder')) {
	        	dispatch(new TestSeeder($user));				
			}

	        return $user;
    	});

    	if (config('settings.register_login')) {
    		Auth::attempt($request->only('email', 'password'));
    	}

	    return $this->responseSuccess('auth.register_success', [
	    	'token' => Auth::getToken(),
	    ]);
    }

    public function confirmation(Request $request) 
    {
		$user = User::where('confirmation_code', $request->route('code'))->first();

		if (!$user) {
			return $this->responseError('messages.incorrect_confirmation_code', 422);
		}

		$user->confirmation_code = null;
		$user->save();	

		Auth::login($user);	

	    return $this->responseSuccess('auth.email_confirmed', [
	    	'token' =>  Auth::getToken()
	    ]);
	}
}
