<?php

namespace App\Http\Controllers\Ajax\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function login(LoginRequest $request) 
    {
        $user = User::where('email', $request->email)->first();

        if ($user->is_expires_password) {
            return $this->responseErrorAuthorization('auth.password_expired');
        }

        if (!$user->confirmed && !config('settings.register_login')) {
            return $this->responseErrorAuthorization('auth.email_not_confirmed');
        }

        if (Auth::attempt($request->only('email', 'password'))) {
            return $this->responseSuccess('actions.login_success', [
                'token' => Auth::getToken(),
            ]);
        } 

        return $this->responseErrorAuthorization('auth.invalid_credentials');
    }
    
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {
        $user = Auth::user();
        
        return $this->response([
            'user' => [
                'name' => $user->name,
                'email' => $user->email,
                'type' => $user->type,
                'permissions' => $user->permissions,
            ],
        ]);
    }

    public function logout()
    {
        Auth::logout();

        return $this->responseSuccess('actions.logout');
    }
}
