<?php

namespace App\Http\Controllers\Ajax\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Auth\PasswordResetRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends BaseResetPasswordController
{
    public function reset(PasswordResetRequest $request)
    {
        return parent::baseReset($request);
    }
}
