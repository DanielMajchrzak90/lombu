<?php

namespace App\Http\Controllers\Ajax\Files;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Dompdf\Dompdf;

class AgreementsPatternsPreviewsController extends Controller
{
    public function show(Request $request, Dompdf $dompdf)
    {
    	$agreement = $request->route('agreement_pattern_model');	
    	$content = $agreement->content;
		$pdf = \PDF::loadView('agreements_patterns.preview', compact('content'));

    	return $pdf->stream();
    }
}
