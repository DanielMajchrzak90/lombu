<?php

namespace App\Http\Controllers\Ajax\Files;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Files\Ck\StoreRequest;
use Storage;
use Acme\Forms\CkFiles\Form;

class CkController extends Controller
{  
    protected $disk;
    protected $attributes = [
        'name', 
        'filename', 
        'url', 
        'relative_url' => 'url',
    ];

    public function __construct()
    {
        $this->disk = Storage::disk('ck_files');
    }

	public function index(Request $request)
    {
        $user = $request->user();
    	$files = $user->files;
        $files = $files->map(function($file) {
            return $file->only($this->attributes);
        });

    	return response()->json([
    		'items' => $files
    	]);
    }

    public function store(StoreRequest $request)
    {
        $files = $request->ck_files;
        $user = $request->user();
        $user->createFiles($files);

    	return response()->json([
    		'msg' => __('actions.files_uploaded'),
    	]);
    }

    public function fields(Request $request, Form $form)
    {        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }

    public function destroy(Request $request)
    {
        $fileId = $request->route('file_id');
        $user = $request->user();
        $file = $user->files()->findOrFail($fileId);
        $file->delete();
        
        return response()->json([
            'msg' => __('actions.file_deleted'),
        ]);
    }
}
