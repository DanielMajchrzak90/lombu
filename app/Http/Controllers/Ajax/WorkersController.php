<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Ajax\Controller;
use App\Http\Requests\Ajax\Worker\StoreRequest;
use App\Http\Requests\Ajax\Worker\UpdateRequest;
use Acme\Forms\Worker\CreateForm;
use Acme\Forms\Worker\EditForm;

class WorkersController extends Controller
{
	protected $worker = [
		'name', 'email',
	];

    protected $forms = [];

    public function __construct(
        CreateForm $createForm,
        EditForm $editForm
    )
    {
        $this->forms = [
            'create' => $createForm,
            'edit' => $editForm,
        ]; 
    }

    public function index(Request $request)
    {
    	$user = $request->user();
        $workers = $user->workers()->search(
            $request->search_phrase, 
            $request->sort_col,
            $request->sort_type
        )->paginate(); 
            
        $workers->getCollection()
        	->transform(function($worker) {
	            return $worker->only($this->worker);
	        });

        return $this->response([
            'items' => $workers,
        ]);
    }

    public function store(StoreRequest $request)
    {
        $data = $request->only([
			'name', 'email', 'password'
		]);
		$data['confirmed'] = true;

    	$user = $request->user();
        $worker = $user->workers()->create($data);
        $worker->lombards = $request->lombards;

        return $this->responseSuccess('actions.worker_created');
    }

    public function edit(Request $request)
    {
        $worker = $request->route('worker');

        return $this->response([
            'item' => $worker->only([
                'name', 'email', 'lombards',
            ]),
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $worker = $request->route('worker');
        $data = $request->only([
            'name', 'email', 'password'
        ]);
		$worker->update($data);
        $worker->lombards = $request->lombards;

        return $this->responseSuccess('actions.worker_updated');
    }

    public function destroy(Request $request)
    {
        $worker = $request->route('worker');
        $worker->delete();

        return $this->responseSuccess('actions.worker_deleted');
    }

    public function fields(Request $request)
    {
        $form = $this->forms[$request->route('type')];
        
        return $this->response([
            'fields' => $form->formFields(),
        ]);
    }
}
