<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Exceptions\ForbiddenException;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class BaseJsonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    protected function failedValidation(Validator $validator) 
    {       
        $errors = [];
        $dotErrors = collect($validator->errors()->getMessages())->map(function($error) {
            return $error[0];
        });

        foreach ($dotErrors as $key => $value) {
            array_set($errors, $key, $value);
        }
                
        throw new HttpResponseException(
            response()->json([
                'errors' => $errors,
            ], 422)
        ); 
    }


    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new ForbiddenException('This action is unauthorized.', 403);
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $names = []; 

        // foreach ($this->rules() as $key => $rule) {
        //     $splitKey = preg_split('/\./', $key);
        //     $attribute = end($splitKey);

        //     $names[$key] = str_replace("_", " ", $attribute);
        // }

        return $names;
    }

}
