<?php

namespace App\Http\Requests\Ajax\Auth;

use App\Http\Requests\BaseJsonRequest;
use App\Models\User;

class PasswordExpiredResetRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'token' => 'required',
            'email' => 'required|email|exists:users,email',
            'password' => 'required|not_the_same_password:'.$this->email.'|string|max:100|'.User::PASSWORD_REGEX,
            'recaptcha' => (\App::environment() === 'production') ? 'required|captcha' : '',
        ];

        return $rules;
    }
}
