<?php

namespace App\Http\Requests\Ajax\Auth;

use App\Http\Requests\BaseJsonRequest;

class PasswordForgotRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|exists:users,email',
            'recaptcha' => (\App::environment() === 'production') ? 'required|captcha' : '',
        ];

        return $rules;
    }
}
