<?php

namespace App\Http\Requests\Ajax\Auth;

use App\Http\Requests\BaseJsonRequest;

class LoginRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string',
            'recaptcha' => (\App::environment() === 'production') ? 'required|captcha' : '',
        ];
    }
}
