<?php

namespace App\Http\Requests\Ajax\Auth;

use App\Http\Requests\BaseJsonRequest;
use App\Models\User;

class RegisterRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email', 
                'unique:users,email',                     
                User::EMAIL_REGEX,
            ],
            'password' => [
                'required', 
                'string', 
                //'confirmed', 
                User::PASSWORD_REGEX
            ],
            'recaptcha' => (\App::environment() === 'production') ? 'required|captcha' : '',
        ];
    }
}
