<?php

namespace App\Http\Requests\Ajax\Profile\PersonalData;

use App\Http\Requests\BaseJsonRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email', 
                Rule::unique('users')->ignore($this->user()->id, 'id'),
            ],
            'name' => 'required|max:100|string',
        ];
    }
}
