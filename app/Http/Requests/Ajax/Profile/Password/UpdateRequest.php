<?php

namespace App\Http\Requests\Ajax\Profile\Password;

use App\Http\Requests\BaseJsonRequest;

class UpdateRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        return [
            'current_password' => [
                'required',              
                'string',  
                'check_password:'.$user->email,
            ],
            'new_password' => [
                'required',                 
                'string',                 
                'not_the_same_password:'.$user->email, 
                'max:100',
                'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+]).{8,}$/',
            ]
        ];
    }
}
