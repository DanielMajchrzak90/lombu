<?php

namespace App\Http\Requests\Ajax\Lombard;

use App\Http\Requests\BaseJsonRequest;

class StoreRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string', 
            // 'street' => 'required_with:street_no,postal_code,city|string', 
            // 'street_no' => 'required_with:street,postal_code,city|string', 
            // 'flat_no' => 'string',
            // 'postal_code' => 'required_with:street,street_no,city|string|regex:/^[0-9]{2}-[0-9]{3}$/',
            // 'city' => 'required_with:street,street_no,postal_code|string', 
        ];
    }
}
