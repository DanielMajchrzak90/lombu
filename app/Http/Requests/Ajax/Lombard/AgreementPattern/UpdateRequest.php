<?php

namespace App\Http\Requests\Ajax\Lombard\AgreementPattern;

use App\Http\Requests\BaseJsonRequest;
use App\Models\AgreementPattern;

class UpdateRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|string',
            'content' => 'required|string|in_content:'.AgreementPattern::FIELDS,
        ];
    }
}
