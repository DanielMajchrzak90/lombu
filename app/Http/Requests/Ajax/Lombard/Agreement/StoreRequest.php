<?php

namespace App\Http\Requests\Ajax\Lombard\Agreement;

use App\Http\Requests\BaseJsonRequest;
use App\Models\AgreementPattern;
use Illuminate\Validation\Rule;

class StoreRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'pesel' => 'required|string',
            'email' => 'email|nullable',
            'id_number_and_series' => 'required|string',
            'end_at' => 'required|date|after:now',
            'amount' => 'required|numeric',
            'cost_per_day' => 'required|numeric',
            'products' => 'required|array|min:1',
            'agreement_pattern_id' => [
                'required',
                Rule::exists('agreements_patterns', 'id')->where(function ($query) {
                    $query->where('lombard_id', $this->lombard->id);
                }),
            ],
            'products' => 'required|array|min:1',
            'products.*.name' => 'required|max:200|string',
            'products.*.market_price' => 'required|numeric',
            'products.*.order' => 'required|numeric|distinct',
        ];

        return $rules;
    }
}
