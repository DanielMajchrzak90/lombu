<?php

namespace App\Http\Requests\Ajax\Lombard\Agreement\Renewal;

use App\Http\Requests\BaseJsonRequest;
use App\Models\AgreementPattern;
use Illuminate\Validation\Rule;

class StoreRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'end_at' => 'required|date|after:now',
            'amount' => 'required|numeric',
        ];

        return $rules;
    }
}
