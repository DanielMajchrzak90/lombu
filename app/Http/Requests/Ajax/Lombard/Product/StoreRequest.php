<?php

namespace App\Http\Requests\Ajax\Lombard\Product;

use App\Http\Requests\BaseJsonRequest;
use Illuminate\Validation\Rule;
use Auth;

class StoreRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $lombard = $this->route('lombard_model');

        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:1000|nullable',
            'market_price' => 'numeric|nullable',
            'purchase_price' => 'numeric|nullable',
            'selling_price' => 'numeric|nullable',
            'category_id' => 'exists:categories,id|nullable',
        ];
    }
}
