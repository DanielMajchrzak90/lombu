<?php

namespace App\Http\Requests\Ajax\Lombard\Employee;

use App\Http\Requests\BaseJsonRequest;
use Illuminate\Validation\Rule;
use Auth;

class StoreRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $lombard = $this->route('lombard_model');

        return [
            'email' =>  [
                'required',
                'exists:users,email',
                'not_owner:' . $lombard->user_id,
                'not_added:' . $lombard->_id,
            ]
        ];
    }
}
