<?php

namespace App\Http\Requests\Ajax\Lombard\Category;

use App\Http\Requests\BaseJsonRequest;
use App\Models\Category;

class StoreRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'parent_id' => 'exists:categories,id|nullable',
        ];
    }
}
