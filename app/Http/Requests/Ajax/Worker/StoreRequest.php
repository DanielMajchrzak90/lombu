<?php

namespace App\Http\Requests\Ajax\Worker;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\BaseJsonRequest;
use App\Models\User;
use Illuminate\Validation\Rule;

class StoreRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'email', 
                'unique:users,email',           
                User::EMAIL_REGEX,
            ],
            'password' => 'required|string|'.User::PASSWORD_REGEX,
            'lombards' => 'required|array|min:1',
            'lombards.*' => [
                'required',
                Rule::exists('lombards', 'id')->where(function ($query) {
                    $query->whereIn('id', $this->user()->lombards->pluck('id'));
                }),
            ],
        ];
    }
}
