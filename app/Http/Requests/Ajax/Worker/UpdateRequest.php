<?php

namespace App\Http\Requests\Ajax\Worker;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use Illuminate\Validation\Rule;
use App\Http\Requests\BaseJsonRequest;

class UpdateRequest extends BaseJsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $worker = $this->route('worker');

        return [
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'email', 
                Rule::unique('users')->ignore($worker->id, 'id'),      
                User::EMAIL_REGEX,
            ],
            'lombards' => 'required|array|min:1',
            'lombards.*' => [
                'required',
                Rule::exists('lombards', 'id')->where(function ($query) {
                    $query->where('user_id', $this->user()->id);
                }),
            ],
        ];
    }
}
