<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AgreementRenewal extends BaseModel
{
    protected $table = 'agreement_renewals';

    protected $fillable = [
        'agreement_id',
    	'end_at',
        'amount',
    ];

    protected $myDateFormat = 'Y-m-d';

    public function agreement()
    {
        return $this->belongsTo(Agreement::class, 'agreement_id');
    }

    public function getAmountAttribute()
    {
        return number_format($this->attributes['amount'], 2);
    }

    public function setEndAtAttribute($value)
    {
        $this->attributes['end_at'] = Carbon::parse($value)->toDateTimeString();
    }

    public function getEndAtAttribute()
    {
        return Carbon::parse($this->attributes['end_at'])->format($this->myDateFormat);
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->toDateTimeString();
    }
}
