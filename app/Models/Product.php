<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends BaseModel
{
    protected $table = 'products';

    protected $fillable = [
    	'agreement_id',
        'lombard_id',
    	'name',
    	'description',
    	'purchase_price',
    	'selling_price',
    	'market_price',
    	'order',
        'category_id',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    protected $searchColumns = ['products.name', 'description', 'categories.name'];

    public function agreement()
    {
        return $this->belongsTo(Agreement::class, 'agreement_id');
    }

    public function getMarketPriceFormattedAttribute()
    {
        return $this->priceFormat($this->attributes['market_price']);
    }

    public function getSellingPriceFormattedAttribute()
    {
        return $this->priceFormat($this->attributes['selling_price']);
    }

    public function getPurchasePriceFormattedAttribute()
    {
        return $this->priceFormat($this->attributes['purchase_price']);
    }

    protected function priceFormat($price)
    {
        return number_format($price, 2);        
    }
}
