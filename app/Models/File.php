<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends BaseModel
{
    protected $table = 'files';

    protected $fillable = [
        'name', 
        'filename', 
        'user_id',
        'disk',
    ];

    protected $path = '/storage/';

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user() 
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function getUrlAttribute()
    {
        return asset($this->relative_url);
    }

    public function getRelativeUrlAttribute()
    {
        return $this->path.$this->disk.'/'.$this->filename;
    }
}
