<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{ 
    protected $searchColumns = [];

	public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public function only($attributes)
    {
        $data = [];
        $attributes[] = 'id';

        foreach ($attributes as $key => $attribute) {            
            $data[(is_numeric($key)) ? $attribute : $key] = $this->$attribute;
        }
        
        return $data;
    }

    public function scopeSearch(
        $query,
        $phrase = null,
        $sortCol = 'id',
        $sortType = 'desc'
    )
    {
        if ($phrase && is_string($phrase) && strlen($phrase) <= 100) {
            $query->WhereRaw("CONCAT_WS(' ',".implode(',', $this->searchColumns).") like '%$phrase%'");
        }

        if ($sortCol 
            && in_array($sortCol, array_merge(
                $this->fillable, 
                $this->searchColumns, 
                ['id', 'created_at']
            ))
            && $sortType 
            && in_array($sortType, ['asc', 'desc'])) {
            $query->orderBy($sortCol, $sortType);
        } else {
            $query->orderBy('id', 'desc');            
        }

        return $query;
    }
}
