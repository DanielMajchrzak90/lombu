<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends BaseModel
{
    protected $table = 'customers';

    protected $fillable = [
        'lombard_id',
    	'first_name',
    	'last_name',
    	'email',
    	'pesel',
    	'id_number_and_series',
    ];

    protected $appends = [
    	'full_name',
    ];

    public function getFullNameAttribute()
    {
    	return $this->first_name.' '.$this->last_name;
    }
}
