<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword;
use Carbon\Carbon;
use DB;
use Storage;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable; 

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'confirmation_code', 
        'confirmed',
        'password_updated_at',
        'employer_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token', 
        'confirmation_code', 
        'confirmed',
    ];

    protected $dates = ['password_updated_at'];

    const PASSWORD_REGEX = '';
    const EMAIL_REGEX = 'regex:/^[^<>\"\'\(\)]+$/i';

    const EMPLOYER_TYPE = 'employer';
    const WORKER_TYPE = 'worker';

    protected $disk = 'ck_files';

    protected $searchColumns = ['name', 'email'];

    public function setConfirmationCodeAttribute($value)
    {
        if ($value === null) {
            $this->attributes['confirmed'] = true;
        }

        $this->attributes['confirmation_code'] = $value;
    }

    public function sendPasswordResetNotification($token)
    { 
        $this->notify(new ResetPassword($token));
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = bcrypt($pass);
        $this->attributes['password_updated_at'] = Carbon::now()->addMonth()->toDateTimeString();
    }

    public function lombards()
    {
        $user = $this->employer ?: $this;

        return $user->hasMany(Lombard::class, 'user_id');
    }

    public function agreements()
    {
        $user = $this->employer ?: $this;

        return $user->hasManyThrough(
            Agreement::class,
            Lombard::class, 
            'user_id', 
            'lombard_id', 
            'id'
        );
    }

    public function employer()
    {
        return $this->belongsTo(User::class, 'employer_id');
    }

    public function files()
    {
        $user = $this->employer ?: $this;

        return $user->morphMany(File::class, 'model');
    }

    public function getIsExpiresPasswordAttribute()
    {
        $days = config('services.password_expiration_days');

        return $this->password_updated_at
            ->addDays($days)
            ->lessThan(Carbon::now());
    }

    public function createFiles($files)
    {
        DB::transaction(function() use ($files) {
            $disk = Storage::disk($this->disk);

            foreach ($files as $file) {     
                $origName = $file->getClientOriginalName();
                $origName = pathinfo($origName, PATHINFO_FILENAME);
                $ext = $file->getClientOriginalExtension();
                $rName = str_random(20);
                $rName = $rName . '.' . $ext;
                $path = $file->path();
                $content = file_get_contents($path);

                $disk->put($rName, $content);

                $this->files()->create([
                    'name' => $origName,
                    'filename' => $rName,
                    'disk' => $this->disk,
                ]);
            }
        });
    }

    public function workers()
    {
        $user = $this->employer ?: $this;

        return $user->hasMany(Worker::class, 'employer_id');
    }

    public function getTypeAttribute()
    {
        return (!$this->employer_id) ? self::EMPLOYER_TYPE : self::WORKER_TYPE;
    }

    public function getIsEmployerAttribute()
    {
        return $this->type == self::EMPLOYER_TYPE;
    }

    public function getIsWorkerAttribute()
    {
        return $this->type == self::WORKER_TYPE;
    }
}
