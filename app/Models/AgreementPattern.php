<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Lombard;

class AgreementPattern extends BaseModel
{
    protected $table = 'agreements_patterns';
    
    protected $fillable = [
        'name', 
        'content', 
    ];

    const FIELDS = 'full_name,pesel,document,created_at,end_at,amount,provision,products';

    public function getPreviewUrlAttribute()
    {
    	return route('agreements-patterns.preview', [
            'locale' => 'pl',
    		'lombard_model' => $this->lombard->id,
    		'agreement_pattern_model' => $this->id,
    	]);
    }

    public function lombard()
    {
        return $this->belongsTo(Lombard::class, 'lombard_id');
    }
}
