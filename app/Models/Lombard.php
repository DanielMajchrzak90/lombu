<?php

namespace App\Models;

use Auth;

class Lombard extends BaseModel
{
    protected $table = 'lombards';

    protected $fillable = [
        'name', 
        'street', 
        'street_no', 
        'flat_no',
        'postal_code',
        'city', 
        'user_id',
    ];

    public function employees()
    {
        return $this->belongsToMany(User::class, 'employees', 'lombard_id', 'user_id');
    }

    public function agreementsPatterns()
    {
        return $this->hasMany(AgreementPattern::class, 'lombard_id');
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'lombard_id');
    }

    public function agreements()
    {
        return $this->hasMany(Agreement::class, 'lombard_id');
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, 'lombard_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'lombard_id');
    }

    public function getAddressAttribute()
    {
    	$address = $this->street . ' ' . $this->street_no;
    	$address .= ($this->flat_no) ? '/' . $this->flat_no : '';
    	$address .= ($this->postal_code) ? ', ' . $this->postal_code : '';
        $address .= ($this->postal_code) ? ' ' . $this->city . '.' : '';

    	return $address;
    }

    public function getIsOwnerAttribute()
    {
        $user = Auth::user();

        if ($user) {
            return $user->id == $this->user_id;
        }
        
        return false;
    }

    public function scopeForUser($query, $userId)
    {
        $query
            ->leftJoin('employees', 'employees.lombard_id', '=', 'lombards.id')
            ->select(
                'employees.user_id as employee_id',
                'lombards.*'
            )
            ->where('lombards.user_id', $userId)
            ->orWhere('employees.user_id', $userId);
    }
}
