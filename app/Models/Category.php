<?php

namespace App\Models;

use DB;
use Acme\EloquentTree\Eloquent\EmbedsRelations;
use Acme\EloquentTree\Builders\EmbedsBuilder;
use Acme\Eloquent\TreeTrait;
use App\Models\BaseModel;

class Category extends BaseModel
{
    use TreeTrait;

    protected $table = 'categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lombard_id', 
        'parent_id', 
        'name', 
        'order', 
    ];

    protected $searchColumns = ['name'];

    public function children()
    {
        return $this->embedsMany(Category::class, 'parent_id');
    }

    public function scopeUpdateDownOrder($query, $newOrder, $oldOrder, $id, $parentId)
    {
        $table = self::getTableName();

        $query->where('parent_id', $parentId)
            ->where('order', '>', $oldOrder)
            ->where('order', '<=', $newOrder)
            ->where('id', '<>', $id)
            ->update([
              'order'=> DB::raw($table.'.order - 1'), 
            ]); 
    }

    public function scopeUpdateUpOrder($query, $newOrder, $oldOrder, $id, $parentId)
    {
        $table = self::getTableName();

        $query->where('parent_id', $parentId)
            ->where('order', '<', $oldOrder)
            ->where('order', '>=', $newOrder)
            ->where('id', '<>', $id)
            ->update([
              'order'=> DB::raw($table.'.order + 1'), 
            ]); 
    }

    public function scopeChangeParentOrder($query, $newOrder, $id, $parentId)
    {
        $table = self::getTableName();

        $query->where('parent_id', $parentId)
            ->where('order', '>=', $newOrder)
            ->where('id', '<>', $id)
            ->update([
              'order'=> DB::raw($table.'.order + 1'), 
            ]); 
    }

    public function scopeResetOrder($query, $parentId)
    {        
        $table = self::getTableName();
       
        DB::statement(DB::raw('SET @rownumber  = -1'));
        $whereClause = ($parentId) ? 
            'where parent_id = '.$parentId :
            'where parent_id is null';
        DB::update('update '.$table.' set '.$table.'.order = (@rownumber:=@rownumber+1) '.$whereClause.' order by '.$table.'.order asc');
    }
}
