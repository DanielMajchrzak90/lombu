<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use NumberFormatter;

class Agreement extends BaseModel
{
    protected $table = 'agreements';

    protected $fillable = [
        'lombard_id',
    	'first_name',
    	'last_name',
    	'email',
    	'pesel',
    	'id_number_and_series',
    	'end_at',
        'amount',
        'cost_per_day',
        'agreement_pattern_id',
    ];

    protected $productAttributes = [
        'name',
        'market_price',
        'order',
    ];

    protected $customerAttributes = [
        'first_name',
        'last_name',
        'email',
        'pesel',
        'id_number_and_series',
    ];

    protected $replaceContent = [
        'full_name',
        'pesel',
        'id_number_and_series' => 'document',
        'end_at',
        'amount',
        'cost_per_day' => 'provision',
        'created_at_formatted' => 'created_at',
        'word_amount',
        'word_provision',
    ];

    protected $searchColumns = [
        'first_name', 
        'last_name',
        'email',
        'pesel',
        'id_number_and_series',
    ];

    protected $myDateFormat = 'Y-m-d';

    const LOAN_TYPE = 'loan';
    const SALE_TYPE = 'sale';
    const PURCHASE_TYPE = 'purchase';

    public function products()
    {
        return $this->hasMany(Product::class, 'agreement_id');
    }

    public function agreementPattern()
    {
        return $this->belongsTo(AgreementPattern::class, 'agreement_pattern_id');
    }

    public function lombard()
    {
        return $this->belongsTo(Lombard::class, 'lombard_id');
    }

    public function renewals()
    {
        return $this->hasMany(AgreementRenewal::class, 'agreement_id');
    }
    
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name).' '. ucfirst($this->last_name);
    }

    public function setEndAtAttribute($value)
    {
        $this->attributes['end_at'] = Carbon::parse($value)->toDateTimeString();
    }

    public function setProducts($products)
    {
        $products = collect($products);
        $existsProducts = $products->filter(function($product) {
            return isset($product['id']);
        });

        $this->deleteProducts($existsProducts->pluck('id'));
        $this->updateProducts($existsProducts);
        $this->createProducts($products->filter(function($product) {
            return !isset($product['id']);
        }));
    }

    protected function getProductData($product)
    {
        $productData = [];

        foreach ($this->productAttributes as $attr) {
            $productData[$attr] = $product[$attr];
        }

        return $productData;
    }

    public function createProducts($requestProducts)
    {
        $products = [];

        foreach ($requestProducts as $product) {
            $product = $this->getProductData($product);
            $product['lombard_id'] = $this->lombard_id;
            $products[] = new Product($product);    
        }

        $this->products()->saveMany($products);
    }

    public function updateProducts($products)
    {
        foreach ($products as $product) {
            $productModel = $this->products()->find($product['id']);
            $productModel->update($this->getProductData($product));
        }
    }

    public function deleteProducts($ids)
    {
        $this->products()->whereNotIn('id', $ids)->delete();
    }

    public function getContentAttribute()
    {
        $agreementPattern = $this->agreementPattern;
        $content = $agreementPattern->content;

        $find = [];
        $replace = [];

        foreach ($this->replaceContent as $key => $attr) {
            $key = (is_numeric($key)) ? $attr : $key;
            $find[] = ':'.$attr;
            $replace[] = $this->$key;
        }

        $products = $this->products;

        $find[] = ':products';
        $replace[] = view('agreements.products', compact('products'));
        $content = str_replace($find, $replace, $content);
        
        return $content;
    }

    public function getAmountAttribute()
    {
        return number_format($this->attributes['amount'], 2);
    }

    public function getCostPerDayAttribute()
    {
        return number_format($this->attributes['cost_per_day'], 2);
    }

    public function setCustomer()
    {        
        $customer = $this
                ->lombard
                ->customers()
                ->where('pesel', $this->pesel)
                ->first();

        $customerData = $this->only($this->customerAttributes);

        if (!$customer) {
            $this
                ->lombard
                ->customers()
                ->create($customerData);
        } else {
            $customer->update($customerData);
        }
    }

    public function getCreatedAtFormattedAttribute()
    {
        return $this->created_at->format($this->myDateFormat);
    }

    public function getEndAtFormattedAttribute()
    {
        return $this->end_at->format($this->myDateFormat);
    }

    public function getWordAmountAttribute()
    {
        return $this->getWordNumber($this->amount);
    }

    public function getWordProvisionAttribute()
    {
        return $this->getWordNumber($this->cost_per_day);
    }

    public function getWordNumber($number)
    {
        $locale = \LaravelLocalization::getCurrentLocale();
        $f = new NumberFormatter($locale, NumberFormatter::SPELLOUT);

        return $f->format($number);        
    }

    public function getPreviewUrlAttribute()
    {
        return route('agreements-preview', [
            'agreement_preview' => $this->id
        ]);
    }
}
