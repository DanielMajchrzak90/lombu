<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends User
{
    public function lombards()
    {
        return $this->belongsToMany(Lombard::class, 'employees', 'user_id', 'lombard_id');
    }

    public function getLombardsAttribute()
    {
    	return $this->lombards()->get()->pluck('id')->toArray();
    }

    public function setLombardsAttribute($ids)
    {
    	return $this->lombards()->sync($ids);
    }
}
