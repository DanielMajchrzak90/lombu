<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include('ajax.php');

$dashboardView = function () {
    return view('dashboard');
};

Route::get('auth/password/reset/{token}', $dashboardView)
	->name('password.reset');
Route::get('auth/password-expired/reset/{token}', $dashboardView)
	->name('password-expired.reset');
Route::get('auth/email/confirmation/{code}', $dashboardView)
	->name('email.confirmation');

Route::middleware(['auth:ajax'])
	->get('agreements-preview/{agreement_preview}', 'AgreementsPreviewController@show')
	->name('agreements-preview');

Route::get('/', $dashboardView)->name('home');

Route::get('{slug}', $dashboardView)
	->where('slug', '.*');
