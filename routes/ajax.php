<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' =>  'ajax',
    'namespace' => 'Ajax',
], function($router) { 
	$routes = function () {
		Route::group([
			'prefix' => 'profile',
		], function() {
			Route::get('personal-data', 'Profile\PersonalDataController@edit');
			Route::patch('personal-data', 'Profile\PersonalDataController@update');
			Route::get('personal-data/fields', 'Profile\PersonalDataController@fields');
			Route::patch('password', 'Profile\PasswordController@update');
			Route::get('password/fields', 'Profile\PasswordController@fields');
		});

		Route::get('workers/fields/{type}', 'WorkersController@fields')
			->where('type', 'create|edit');
		Route::resource('workers', 'WorkersController', [
			'except' => [
			    'show'
			]
		]);

		Route::get('lombards/fields/{type}', 'LombardsController@fields')
			->where('type', 'create|edit');
		Route::resource('lombards', 'LombardsController');

		Route::group([
		    'prefix' => 'lombards/{lombard}',
		    'namespace' => 'Lombards',
		], function($router) { 

			Route::get('products/fields', 'ProductsController@fields');
			Route::get('agreements/fields', 'AgreementsController@fields');
			Route::get('categories/fields', 'CategoriesController@fields');

			Route::get('agreements-patterns', 
				'AgreementsPatternsController@index');
			Route::post('agreements-patterns', 
				'AgreementsPatternsController@store');
			Route::get('agreements-patterns/{agreement_pattern}/edit', 
				'AgreementsPatternsController@edit');
			Route::patch('agreements-patterns/{agreement_pattern}', 
				'AgreementsPatternsController@update');
			Route::delete('agreements-patterns/{agreement_pattern}', 
				'AgreementsPatternsController@destroy');			

			Route::get('categories', 'CategoriesController@index');
			Route::get('categories/tree', 'CategoriesController@tree');
			Route::post('categories', 'CategoriesController@store');
			Route::get('categories/{category}/edit', 'CategoriesController@edit');
			Route::patch('categories/{category}', 'CategoriesController@update');
			Route::patch('categories/{category}/nestable', 'CategoriesController@updateNestable');
			Route::delete('categories/{category}', 'CategoriesController@destroy');

			Route::resource('agreements', 'AgreementsController', [
				'except' => [
				    'show'
				]
			]);
			Route::resource(
				'agreements/{agreement}/renewals', 
				'Agreements\RenewalsController', [
				'except' => [
				    'show', 'edit', 'create'
				]
			]);

			Route::resource('products', 'ProductsController', [
				'except' => [
				    'show'
				]
			]);
		});

		Route::group([
		    'prefix' => 'lombards',
		    'namespace' => 'Lombards',
		], function($router) { 
			Route::get(
				'agreements-patterns/fields', 
				'AgreementsPatternsController@fields'
			);
			Route::get(
				'agreements/renewals/fields', 
				'Agreements\RenewalsController@fields'
			);
		});
		
		Route::group([
		    'prefix' => 'files',
		    'namespace' => 'Files',
		], function($router) { 
			Route::get(
				'lombards/{lombard_model}/agreements-patterns/{agreement_pattern_model}/preview', 
				'AgreementsPatternsPreviewsController@show'
			)->name('agreements-patterns.preview');

			Route::get('ck', 'CkController@index');
			Route::post('ck', 'CkController@store');
			Route::delete('ck/{file_id}', 'CkController@destroy');
			Route::get('ck/fields', 'CkController@fields');
		});
	};

	Route::prefix('employer')
		->middleware(['auth:ajax', 'can:isEmployer'])
		->group($routes);

	Route::prefix('worker')
		->middleware(['auth:ajax_worker', 'can:isWorker'])
		->group($routes);

	Route::prefix('auth')->group(function() {
		Route::middleware(['auth:ajax'])->group(function() {
			Route::get('user', 'Auth\LoginController@getAuthenticatedUser');
			Route::get('logout', 'Auth\LoginController@logout');
		});
		Route::get('fields/{type}', 'Auth\FormFieldsController@fields')
			->where('type', 'login|register|forgot_password|reset_password');
		Route::post('login', 'Auth\LoginController@login');
		Route::post('register', 'Auth\RegisterController@register');
		Route::post('/password/forgot', 'Auth\ForgotPasswordController@sendResetLink');
		Route::post('/password/expired', 'Auth\ExpiredPasswordController@sendResetLink');
		Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
		Route::post('/password-expired/reset', 'Auth\ResetExpiredPasswordController@reset');
		Route::post('/confirmation/{code}', 'Auth\RegisterController@confirmation');
	});
});
