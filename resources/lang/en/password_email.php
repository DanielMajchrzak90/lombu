<?php 

return [
	'topic' => 'Reset Password Notification',
	'hello' => 'Hello',
	'top_text' => 'You are receiving this email because I received a password reset request for your account.',
	'button_text' => 'Reset Password',
	'bottom_text' => 'If you did not request a password reset, no further action is required.',
	'thanks' => 'Thanks',
	'trouble' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser: [:actionURL](:actionURL)',
];