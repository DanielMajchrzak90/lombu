<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'registration' => 'Registration',
    'activate_account' => 'Activate the account',
    'thanks' => 'thanks',
    'all_rights_reserved' => 'All rights reserved',
    'email_confirmed' => 'After :seconds second You will be redirect to the admin panel.',
    'password_expired' => 'Your password has been expired, please set new password.',
    'invalid_credentials' => 'Invalid credentials',
    'email_not_confirmed' => 'Email is not confirmed. Please confirm before login.',
    'confirmed_success' => 'Confirmed success',

];
