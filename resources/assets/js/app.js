require('./bootstrap');

import Vue from 'vue';
import Vuex from 'vuex'
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import VueBreadcrumbs from 'vue-2-breadcrumbs';
import PortalVue from 'portal-vue'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueAxios, window.axios) 
//Vue.use(VueBreadcrumbs)
Vue.use(PortalVue)

require('./icons');
require('./components');

import App from './App.vue';

import store from './store/index'
import {router} from "./router.js"
import EventBus from './event-bus';

const app = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  mounted() {
    var self = this
  	EventBus.$on('error', function(error) {
  		console.error(error)
  		if (error.hasOwnProperty('response')) {  			
	  		console.error(error.response)
	  		var response = error.response
	  		toastr.error(response.data.error)

	  		if (response.status == 401) {
	  			self.$router.push({name: 'login'})
	  		}
  		}
  	})
  }
});
