import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "./jwt.service";

class Service {
  constructor() {
    let service = axios.create({});
    this.service = service;
    this.service.defaults.baseURL = '/ajax/' 
  }

  setHeader() {
    this.service.defaults.headers.common[
    'X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    this.service.defaults.headers.common[
      "auth"
    ] = 'Bearer ' + JwtService.getToken();
  }

  prefix(prefix) {
    this.service.defaults.baseURL = prefix
  }

  errorHandle(myHandle) {
    this.service.interceptors.response.use(null, function(error) {

      if (error.hasOwnProperty('response') & error.response != undefined) {

        var response = error.response
        console.log(response)
        myHandle(response)     

        if (_.includes([401, 429, 400, 403], response.status)) {
          toastr.error(response.data.msg)    
        }
      }

      return Promise.reject(error);
    })
  }

  get(slug) {
    return this.service.get(slug)
  }

  post(slug, params) {
    return this.service.post(slug, params)
  }

  update(slug, params) {
    if (params instanceof FormData) {
      params.append("_method", 'PATCH');   
    } else {
      params._method = 'PATCH'
    }

    return this.service.post(slug, params)
  }

  delete(slug) {
    return this.service.post(slug, {_method: 'DELETE'});
  }
}

export default Service;