import ProfilePersonalDataForm from './../views/panel/profile/personal_data/Form.vue';
import ProfilePasswordForm from './../views/panel/profile/password/Form.vue';
import ProfileDashboard from './../views/panel/profile/Dashboard.vue';

import WorkersIndex from './../views/panel/workers/Index.vue';
import WorkersCreate from './../views/panel/workers/Create.vue';
import WorkersEdit from './../views/panel/workers/Edit.vue';

import Panel from './../views/panel/Panel.vue';

import PageNotFound from './../views/errors/PageNotFound.vue';
import Forbidden from './../views/errors/Forbidden.vue';
import store from './../store/index'

import LombardRoutes from './lombard'

const PERSONAL_DATA = require('./../store/types/profile/personal_data')
const WORKER = require('./../store/types/worker')

export default {
  name: 'panel',
  path: '/panel', 
  redirect: to => {
  	return to.params.locale + '/panel/lombards'
  },
	component: Panel, 
  meta: { requiresAuth: true },
  children: [
	  { 
	  	path: "404", 
	  	component: PageNotFound,
	  },
		{
		  name: 'profile',
		  path: 'profile', 
  		redirect: 'profile/personal-data',
			component: ProfileDashboard, 
	    meta: { requiresAuth: true },
      children: [
				{
				  name: 'profile-personal-data',
				  path: 'personal-data', 
				  component: ProfilePersonalDataForm, 
	        meta: { 
	        	requiresAuth: true,
				  	async action(params, query) {
				      await store.dispatch(PERSONAL_DATA.EDIT, query)    
				  	}, 
				  },
				},
				{
				  name: 'profile-password',
				  path: 'password', 
				  component: ProfilePasswordForm, 
	        meta: { 
	        	requiresAuth: true, 
	        },
				},
      ],
		},
		{
		  name: 'workers',
		  path: 'workers', 
		  component: WorkersIndex, 
			meta: { 
				requiresAuth: true,
		  	async action(params, query) {
		      await store.dispatch(WORKER.INDEX, query)    
		  	},
			},
		},
		{
		  name: 'workers-create',
		  path: 'workers/create', 
		  component: WorkersCreate, 
			meta: { 
				requiresAuth: true,
		  	async action() {
					store.commit(WORKER.SET_ITEM, {})   
		  	},
		  },
		},
		{
		  name: 'workers-edit',
		  path: 'workers/:worker_id/edit', 
		  component: WorkersEdit, 
			meta: { 
				requiresAuth: true,
		  	async action(params) {
				  await store.dispatch(WORKER.EDIT, params.worker_id)   
		  	},
		  },
		},
		...LombardRoutes,
  ],
}