import lombardsIndex from './../views/panel/lombards/Index.vue';
import lombardsCreate from './../views/panel/lombards/Create.vue';
import lombardsEdit from './../views/panel/lombards/Edit.vue';
import lombardsShow from './../views/panel/lombards/Show.vue';

import agreementPatternsIndex from './../views/panel/lombards/show/agreement_patterns/Index.vue';
import agreementPatternsForm from './../views/panel/lombards/show/agreement_patterns/Form.vue';

import categoriesDashboard from './../views/panel/lombards/show/categories/Dashboard.vue';
import categoriesIndex from './../views/panel/lombards/show/categories/Index.vue';
import categoriesTree from './../views/panel/lombards/show/categories/Tree.vue';
import categoriesForm from './../views/panel/lombards/show/categories/Form.vue';

import agreementsIndex from './../views/panel/lombards/show/agreements/Index.vue';
import agreementsForm from './../views/panel/lombards/show/agreements/Form.vue';

import productsIndex from './../views/panel/lombards/show/products/Index.vue';
import productsForm from './../views/panel/lombards/show/products/Form.vue';
import store from './../store/index'

const LOMBARD = require('./../store/types/lombard')
const AGREEMENT_PATTERN = require('./../store/types/lombard/agreement_pattern')
const CATEGORY = require('./../store/types/lombard/category')
const AGREEMENT = require('./../store/types/lombard/agreement')
const PRODUCT = require('./../store/types/lombard/product')

const formAction =  async (resource, param, emptyResource = {}) => {
  if (param == undefined) {   
    store.commit(resource.SET_ITEM, emptyResource)     
  } else {  
    await store.dispatch(resource.EDIT, param)    
  }
}

export default [
	{
	  name: 'lombards',
	  path: 'lombards', 
	  component: lombardsIndex, 
		meta: { 
			requiresAuth: true,
	  	async action(params, query) {
	      await store.dispatch(LOMBARD.INDEX, query)    
	  	},
		},
	},
	{
	  name: 'lombards-create',
	  path: 'lombards/create', 
	  component: lombardsCreate, 
		meta: { 
			requiresAuth: true,
	  	async action() {
				store.commit(LOMBARD.SET_ITEM, {})   
	  	},
	  },
	},
	{
	  name: 'lombards-edit',
	  path: 'lombards/:lombard_id/edit', 
	  component: lombardsEdit, 
		meta: { 
			requiresAuth: true,
	  	async action(params) {
			  await store.dispatch(LOMBARD.EDIT, params.lombard_id)   
	  	},
	  },
	},
	{
	  name: 'lombards-show',
	  path: 'lombards/:lombard_id', 
	  component: lombardsShow, 
		props: true,
		meta: { 
			requiresAuth: true,
	  	async action(params) {
	  		var lombard = store.getters[LOMBARD.SHOW];
	  		if (lombard.id != params.lombard_id) {
			  	await store.dispatch(LOMBARD.SHOW, params.lombard_id)
	  		}   
	  	},
	  },
	  redirect: { name: 'products' },
		children: [
			{
			  name: 'agreements-patterns',
			  path: 'agreements-patterns', 
			  component: agreementPatternsIndex, 
				meta: { 
					requiresAuth: true,
			  	async action(params, query) {
			      await store.dispatch(AGREEMENT_PATTERN.INDEX, query)    
			  	},
			  },
				props: true,
			},
			{
			  name: 'agreements-patterns-form',
			  path: 'agreements-patterns/:agreement_pattern_id?/form', 
			  component: agreementPatternsForm, 
				meta: { 
					requiresAuth: true,
			  	async action(params) {
			  		await formAction(AGREEMENT_PATTERN, params.agreement_pattern_id)
			  	},
			  },
				props: true,
			},
			{
				path: 'categories',
				component: categoriesDashboard,
				children: [
					{
					  name: 'categories',
					  path: 'tree', 
					  component: categoriesTree, 
						meta: { 
							requiresAuth: true,
					  	async action(params) {
					      await store.dispatch(CATEGORY.TREE)    
					  	},
					  },
						props: true,
					},
					{
					  name: 'categories-table',
					  path: 'table', 
					  component: categoriesIndex, 
						meta: { 
							requiresAuth: true,
					  	async action(params, query) {
					  		console.log(query)
					      await store.dispatch(CATEGORY.INDEX, query)    
					  	},
					  },
						props: true,
					},
				]
			},
			{
			  name: 'categories-form',
			  path: 'categories/:category_id?/form', 
			  component: categoriesForm, 
				meta: { 
					requiresAuth: true,
			  	async action(params) {
			  		await formAction(CATEGORY, params.category_id)
			  	},
				},
				props: true,
			},
			{
			  name: 'agreements',
			  path: 'agreements', 
			  component: agreementsIndex, 
				meta: { 
					requiresAuth: true,
			  	async action(params, query = {}) {
			      await store.dispatch(AGREEMENT.INDEX, query)    
			  	},
			  },
				props: true,
			},
			{
			  name: 'agreements-form',
			  path: 'agreements/:agreement_id?/form', 
			  component: agreementsForm, 
				meta: { 
					requiresAuth: true, 							
			  	async action(params) {
			  		await formAction(AGREEMENT, params.agreement_id)
			  	},
				},
				props: true,
			},
			{
			  name: 'products',
			  path: 'products', 
			  component: productsIndex, 
				meta: { 
					requiresAuth: true,
			  	async action(params, query = {}) {
			      await store.dispatch(PRODUCT.INDEX, query)    
			  	},
			  },
				props: true,
			},
			{
			  name: 'products-form',
			  path: 'products/:product_id?/form', 
			  component: productsForm, 
				meta: { 
					requiresAuth: true, 							
			  	async action(params) {
			  		await formAction(PRODUCT, params.product_id)
			  	},
				},
				props: true,
			},
		],
	}
]