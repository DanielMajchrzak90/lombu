import Auth from './../views/auth/Auth.vue';
import Login from './../views/auth/Login.vue';
import Register from './../views/auth/Register.vue';
import Confirmation from './../views/auth/Confirmation.vue';
import PasswordForgot from './../views/auth/PasswordForgot.vue';
import PasswordExpired from './../views/auth/PasswordExpired.vue';
import PasswordReset from './../views/auth/PasswordReset.vue';
import PasswordExpiredReset from './../views/auth/PasswordExpiredReset.vue';
import store from './../store/index'

export default {
	  name: 'auth',
	  path: '/auth', 
    redirect: to => {
    	return '/' + to.params.locale + '/auth/login'
    },
		component: Auth, 
    children: [
			{
			  name: 'login',
			  path: 'login', 
			  component: Login, 
				meta: { requiresGuest: true },
			},
			{
			  name: 'register',
			  path: 'register', 
			  component: Register, 
				meta: { requiresGuest: true },
			},
			{
			  name: 'confirmation',
			  path: 'email/confirmation/:code', 
			  component: Confirmation, 
			},
			{
			  name: 'password-forgot',
			  path: 'password/forgot', 
			  component: PasswordForgot, 
				meta: { requiresGuest: true },
			},
			{
			  name: 'password-expired',
			  path: 'password/expired', 
			  component: PasswordExpired, 
				meta: { requiresGuest: true },
			},
			{
			  name: 'password-reset',
			  path: 'password/reset/:token', 
			  component: PasswordReset, 
				meta: { requiresGuest: true },
			},
			{
			  name: 'password-expired-reset',
			  path: 'password-expired/reset/:token', 
			  component: PasswordExpiredReset, 
				meta: { requiresGuest: true },
			},
    ]
  }