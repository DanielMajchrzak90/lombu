import personalData from './personal_data.module'

import {
  PREFIX,
  SET_FIELDS,
  FIELDS,
} from './../types/types.js'

const state = {
  ...personalData.state
}  

const getters = {
   ...personalData.getters,   
  [PREFIX] (state, getters, rootState, rootGetters) {
    return '/profile/password'
  },
};

const actions = {
   ...personalData.actions,
};
const mutations = {
   ...personalData.mutations,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}