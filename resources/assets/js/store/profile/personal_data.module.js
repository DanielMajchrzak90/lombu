import crud from './../crud.module'

import {
  PREFIX,
  SET_FIELDS,
  FIELDS,
  EDIT,
  UPDATE,
  SET_ITEM
} from './../types/types.js'

const state = {
  item: {},
  fields: [],
}  

const getters = {
   ...crud.getters,   
  [PREFIX] (state, getters, rootState, rootGetters) {
    return '/profile/personal-data'
  },
};

const actions = {
  async [EDIT] ({commit, getters}, id) {
    var response = await getters.getAxios.get(getters[PREFIX])

    commit(SET_ITEM, response.data.item)
  },
  async [UPDATE] ({commit, getters, rootState, dispatch}, credentials) {

    var response = await getters.getAxios.update(
      getters[PREFIX],
      credentials
    )

    return response.data.msg
  },
  async [FIELDS] ({commit, getters}) {
    var url = getters[PREFIX]+'/fields'
    
    var response = await getters.getAxios.get(url)

    commit(SET_FIELDS, response.data.fields)
  },
};
const mutations = {
   ...crud.mutations,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}