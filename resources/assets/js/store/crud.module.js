import ApiService from "./../common/api.service";

import {
  INDEX,
  CREATE,
  SHOW,
  EDIT,
  STORE,
  UPDATE,
  DESTROY,
  FIELDS,
  SET_ITEMS,
  SET_ITEM,
  UNSET_ITEM,
  SET_FIELDS,
  PREFIX,
  FIELDS_PREFIX,
} from './types/types.js'

const state = {
  items: {
    data: []
  },
  item: {},
  fields: [],
}

const getters = {
  [INDEX] (state) {
    return state.items
  }, 
  [SHOW] (state) {
    return state.item
  }, 
  [FIELDS] (state) {
    return state.fields
  }, 
  [PREFIX] (state) {
    return ''
  },
  [FIELDS_PREFIX] (state, getters, rootState, rootGetters) {
    return getters[PREFIX]
  },
  getAxios(state, getters, rootState, rootGetters) {
    var api = new ApiService
    api.setHeader()
    var locale = rootGetters.getLocale
    var user = rootGetters['auth/currentUser']
    api.prefix('/'+locale+'/ajax/'+user.type)

    return api
  },
}

const actions = {
  async [INDEX] ({commit, getters}, query = {}) {
    var url = getters[PREFIX]
    
    var response = await getters.getAxios.get(url+'?'+$.param(query))
    
    commit(SET_ITEMS, response.data.items)
  },
  async [CREATE] ({commit, getters}, view) {
    commit(SET_ITEM, {})
  },
  async [SHOW] ({commit, getters}, id) {
    var response = await getters.getAxios.get(getters[PREFIX]+'/'+id)

    console.log(response)
    commit(SET_ITEM, response.data.item)
  },
  async [EDIT] ({commit, getters}, id) {
    var response = await getters.getAxios.get(getters[PREFIX]+'/'+id+'/edit')

    commit(SET_ITEM, response.data.item)
  },
  async [STORE] ({commit, getters}, credentials) {
    var response = await getters.getAxios.post(getters[PREFIX], credentials)

    return response.data.msg
  },
  async [UPDATE] ({commit, getters, rootState, dispatch}, credentials) {

    var response = await getters.getAxios.update(
      getters[PREFIX]+'/'+getters.show.id, 
      credentials
    )

    return response.data.msg
  },
  async [DESTROY] ({commit, getters, dispatch}, query) {
    var response = await getters.getAxios.delete(getters[PREFIX]+'/'+query.id)

    await dispatch(INDEX , {
      search_phrase: query.search_phrase,
      sort_col: query.sort_col,
      sort_type: query.sort_type,
      page: query.page,
    })

    return response.data.msg
  },
  async [FIELDS] ({commit, getters}, view) {
    var url = getters[FIELDS_PREFIX]+'/fields/'+view
    
    var response = await getters.getAxios.get(url)

    commit(SET_FIELDS, response.data.fields)
  },
}

const mutations = {
  [SET_ITEMS] (state, items) {
    state.items = items
  },
  [SET_ITEM] (state, item) {
    state.item = item
  },
  [UNSET_ITEM] (state, id) {
    var index = _.findIndex(state.items.data, function(o) { return o.id == id; });
    
    if (index != -1) {
       state.items.data.splice(index, 1)
    }
  },
  [SET_FIELDS] (state, fields) {
    state.fields = fields
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}