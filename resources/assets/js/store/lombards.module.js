import crud from './crud.module'

import agreementsPatterns from './lombards/agreements_patterns.module'
import categories from './lombards/categories.module'
import agreements from './lombards/agreements.module'
import products from './lombards/products.module'


import {
  PREFIX
} from './types/types.js'


const state = {
   ...crud.state,
};
const getters = {
   ...crud.getters,   
  [PREFIX] (state) {
    return 'lombards'
  },
};
const actions = {
   ...crud.actions,
};
const mutations = {
   ...crud.mutations,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
  modules: {
    agreementsPatterns,
    categories,
    agreements,
    products,
  }
}