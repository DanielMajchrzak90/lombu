const NAMESPACE = 'profile/password/'
const TYPES = require('./../types.js')

export const FIELDS = NAMESPACE + TYPES.FIELDS
export const SET_ITEM = NAMESPACE + TYPES.SET_ITEM

export const SHOW = NAMESPACE + TYPES.SHOW
export const UPDATE = NAMESPACE + TYPES.UPDATE
export const EDIT = NAMESPACE + TYPES.EDIT