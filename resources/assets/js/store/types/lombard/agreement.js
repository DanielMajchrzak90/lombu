const NAMESPACE = 'lombards/agreements/'
const TYPES = require('./../types.js')

export const FIELDS = NAMESPACE + TYPES.FIELDS
export const SET_ITEM = NAMESPACE + TYPES.SET_ITEM

export const INDEX = NAMESPACE + TYPES.INDEX
export const CREATE = NAMESPACE + TYPES.CREATE
export const STORE = NAMESPACE + TYPES.STORE
export const SHOW = NAMESPACE + TYPES.SHOW
export const EDIT = NAMESPACE + TYPES.EDIT
export const UPDATE = NAMESPACE + TYPES.UPDATE
export const DESTROY = NAMESPACE + TYPES.DESTROY