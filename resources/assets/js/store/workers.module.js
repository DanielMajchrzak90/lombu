import crud from './crud.module'

import {
  PREFIX
} from './types/types.js'


const state = {
   ...crud.state,
};
const getters = {
   ...crud.getters,   
  [PREFIX] (state) {
    return 'workers'
  },
};
const actions = {
   ...crud.actions,
};
const mutations = {
   ...crud.mutations,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}