import crud from './crud.module'

import personalData from './profile/personal_data.module'
import password from './profile/password.module'

import {
  PREFIX
} from './types/types.js'


const state = {};
const getters = {};
const actions = {};
const mutations = {}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
  modules: {
    personalData,
    password,
  }
}