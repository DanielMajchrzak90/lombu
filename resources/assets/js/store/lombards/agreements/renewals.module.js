import crud from './../../crud.module'

import {
  PREFIX,
  SET_FIELDS,
  UPDATE,
  FIELDS,
  FIELDS_PREFIX,
} from './../../types/types.js'

const state = {
   ...crud.state,
};
const getters = {
   ...crud.getters,   
  [PREFIX] (state, getters, rootState, rootGetters) {
    var lombard = rootGetters[require('./../../types/lombard.js').SHOW]
    var agreement = rootGetters[require('./../../types/lombard/agreement.js').SHOW]

    return '/lombards/'+lombard.id+'/agreements/'+agreement.id+'/renewals'
  },
};
const actions = {
   ...crud.actions,
  async [UPDATE] ({commit, getters, rootState, dispatch}, item) {

    var response = await getters.getAxios.update(
      getters[PREFIX]+'/'+item.id, 
      item
    )

    return response.data.msg
  },
  async [FIELDS] ({commit, getters}) {
    var url = '/lombards/agreements/renewals/fields'
    
    var response = await getters.getAxios.get(url)

    commit(SET_FIELDS, response.data.fields)
  },
};
const mutations = {
   ...crud.mutations,   
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}