import crud from './../crud.module'

import renewals from './agreements/renewals.module'

import {
  PREFIX,
  SET_FIELDS,
  FIELDS,
  FIELDS_PREFIX,
} from './../types/types.js'

import {
  SHOW
} from './../types/lombard.js'

const state = {
   ...crud.state,
};
const getters = {
   ...crud.getters,   
  [PREFIX] (state, getters, rootState, rootGetters) {
    var lombard = rootGetters[SHOW]

    return '/lombards/'+lombard.id+'/agreements'
  },
};
const actions = {
   ...crud.actions,
  async [FIELDS] ({commit, getters}) {
    var url = getters[PREFIX]+'/fields'
    
    var response = await getters.getAxios.get(url)

    commit(SET_FIELDS, response.data.fields)
  },
};
const mutations = {
   ...crud.mutations,   
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
  modules: {
    renewals,
  }
}