import crud from './../crud.module'

import {
  PREFIX,
  SET_FIELDS,
  FIELDS,
  FIELDS_PREFIX,
  TREE,
  SET_TREE,
  UPDATE_NESTABLE
} from './../types/types.js'

import {
  SHOW
} from './../types/lombard.js'

const state = {
   ...crud.state,
  tree: [],
};
const getters = {
   ...crud.getters,   
   [TREE] (state) {
    return state.tree
  },
  [PREFIX] (state, getters, rootState, rootGetters) {
    var lombard = rootGetters[SHOW]

    return '/lombards/'+lombard.id+'/categories'
  },
};
const actions = {
   ...crud.actions,
  async [TREE] ({commit, getters}) {
    var url = getters[PREFIX]+'/tree'
    
    var response = await getters.getAxios.get(url)

    commit(SET_TREE, response.data.tree)
  },
  async [UPDATE_NESTABLE] ({commit, getters, rootState, dispatch, rootGetters}, credentials) {
    var url = getters[PREFIX]+'/'+credentials.id+'/nestable'

    var response = await getters.getAxios.update(url, credentials)

    return response.data.msg
  },
  async [FIELDS] ({commit, getters}) {
    var url = getters[PREFIX]+'/fields'
    
    var response = await getters.getAxios.get(url)

    commit(SET_FIELDS, response.data.fields)
  },
};
const mutations = {
   ...crud.mutations,
  [SET_TREE] (state, tree) {
    state.tree = tree
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}