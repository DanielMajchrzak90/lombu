import crud from './../crud.module'

import {
  PREFIX,
  SET_FIELDS,
  FIELDS,
} from './../types/types.js'

import {
  SHOW
} from './../types/lombard.js'

const state = {
   ...crud.state,
  items: [],
};
const getters = {
   ...crud.getters,   
  [PREFIX] (state, getters, rootState, rootGetters) {
    var lombard = rootGetters[SHOW]

    return '/lombards/'+lombard.id+'/agreements-patterns'
  },
};
const actions = {
   ...crud.actions,
  async [FIELDS] ({commit, getters}) {
    var url = '/lombards/agreements-patterns/fields'
    
    var response = await getters.getAxios.get(url)

    commit(SET_FIELDS, response.data.fields)
  },
};
const mutations = {
   ...crud.mutations,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}