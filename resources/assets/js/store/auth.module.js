import ApiService from "./../common/api.service";
import JwtService from "./../common/jwt.service";

import {
  PENDING_TOGGLE,
  PURGE_ERRORS,
  SET_ERRORS,
  SET_USER,
  PURGE_AUTH,
  CHECK_AUTH,
  LOGOUT,
  LOGIN,
  REGISTER,
  CONFIRM,
  RESET_PASSWORD,
  RESET_EXPIRED_PASSWORD,
  SEND_RESET_EXPIRED_LINK,
  SEND_RESET_LINK,
  FIELDS,
  SET_FIELDS,
  CURRENT_USER,

} from './types/types.js'

const state = {
  fields: [],
  user: null,
  isAuthenticated: false,
}

const getters = {
  errors (state) {
    return state.errors
  }, 
  pending (state) {
    return state.pending
  }, 
  [CURRENT_USER](state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated
  },
  [FIELDS](state) {
    return state.fields
  },
  getAxios(state, getters, rootState, rootGetters) {
    var api = new ApiService
    api.setHeader()
    var locale = rootGetters.getLocale
    api.prefix('/'+locale+'/ajax/auth')

    return api
  },
  [FIELDS] (state) {
    return state.fields
  }, 
}

const actions = {
  async [LOGIN] ({commit, rootState, getters}, credentials) {
    commit(PURGE_ERRORS)

    credentials.recaptcha = $('textarea[name="g-recaptcha-response"]').val()
    var response = await getters.getAxios.post('/login', credentials, {
      withCredentials: true
    })

    JwtService.saveToken(response.data.token);
    
    return response.data.msg 
  },
  async [REGISTER] ({commit, rootState, getters}, credentials) {
    commit(PURGE_ERRORS)

    credentials.recaptcha = $('textarea[name="g-recaptcha-response"]').val()

    console.log(credentials)
    var response = await getters.getAxios.post('/register', credentials, {
      withCredentials: true
    })

    if (response.data.token) {
      JwtService.saveToken(response.data.token);
    }
  },
  async [LOGOUT] ({getters, commit}) {
    var response = await getters.getAxios.get('/logout')
    
    await commit(PURGE_AUTH)
  },
  async [CHECK_AUTH] ({ commit, getters, rootState }) {
    if (JwtService.getToken()) {
        try {
          var api = getters.getAxios
          api.setHeader()
          var response = await api.get('/user')
          
          commit(SET_USER, response.data.user)

        } catch (error) {
          console.log(error)
          commit(PURGE_AUTH)
        }

        rootState.errorResponse = response
    } else {
      commit(PURGE_AUTH);
    }
  },
  async [SEND_RESET_LINK] ({ commit, getters }, credentials) {
    commit(PURGE_ERRORS)

    credentials.recaptcha = $('textarea[name="g-recaptcha-response"]').val()

    var response = await getters.getAxios.post('/password/forgot', credentials)

    return response.data.msg 
  },
  async [SEND_RESET_EXPIRED_LINK] ({ commit, getters }, credentials) {
    commit(PURGE_ERRORS)

    credentials.recaptcha = $('textarea[name="g-recaptcha-response"]').val()

    var response = await getters.getAxios.post('/password/expired', credentials)

    return response.data.msg 
  },
  async [RESET_PASSWORD] ({ commit, getters }, credentials) {
    commit(PURGE_ERRORS)

    credentials.recaptcha = $('textarea[name="g-recaptcha-response"]').val()

    var response = await getters.getAxios.post('/password/reset', credentials)
    JwtService.saveToken(response.data.token);

    return response.data.msg
  },
  async [RESET_EXPIRED_PASSWORD] ({ commit, getters }, credentials) {
    commit(PURGE_ERRORS)

    credentials.recaptcha = $('textarea[name="g-recaptcha-response"]').val()

    var response = await getters.getAxios.post('/password-expired/reset', credentials)
    JwtService.saveToken(response.data.token);

    return response.data.msg
  },
  async [CONFIRM] ({ commit, getters }, code) {
    commit(PURGE_ERRORS)

    var response = await getters.getAxios.post('/confirmation/' + code)
    JwtService.saveToken(response.data.token);

    return response.data.msg
  },
  async [FIELDS] ({commit, getters}, view) {
    var url = 'fields/'+view
    
    var response = await getters.getAxios.get(url)

    commit(SET_FIELDS, response.data.fields)
  },
}

const mutations = {
  [PENDING_TOGGLE] (state) {
    state.pending = !state.pending
  },
  [SET_ERRORS] (state, errors) {
    state.errors = errors
  },
  [SET_USER] (state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
  },
  [PURGE_AUTH] (state) {
    state.isAuthenticated = false
    state.user = null
    state.errors = {}
    JwtService.destroyToken()
  },
  [PURGE_ERRORS] (state) {
    state.errors = {}
  },
  [SET_FIELDS] (state, fields) {
    state.fields = fields
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}