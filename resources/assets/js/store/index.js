import ApiService from "./../common/api.service";

import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth.module'
import profile from './profile.module'
import lombards from './lombards.module'
import workers from './workers.module'
import ckFiles from './ck_files.module'

Vue.use(Vuex)

import Lang from 'lang.js'
import messages from './../messages'

const lang = new Lang({ 
	messages: messages,
  locale: 'en',
  fallback: 'en' 
})

import {
  SET_LOCALE,
} from './types.js'

export default new Vuex.Store({
  namespaced: true,
  watch: {
  },
	state: {
    lang: lang,
		errorResponse: null,
	},
	getters: {
	  lang (state) {
	    return state.lang
	  }, 
	  getLocale(state) {
	    return state.lang.getLocale()
	  },
	  getLocales(state) {
	    return state.locales
	  },
	  errorResponse(state) {
	    return state.errorResponse
	  },
	  appName() {
	  	return $('#app-name').val()
	  },
	},
	actions: {
	},
	mutations: {
		[SET_LOCALE] (state, locale) {
			state.lang.setLocale(locale)
		},
	},
  modules: {
    auth,
    profile,
    lombards,
    ckFiles,
    workers,
  }
})