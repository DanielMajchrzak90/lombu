import EventBus from './../event-bus';

export default {
	data() {
		return {
			pending: false,
			errors: {},
      reloadKey: 0,
		}
	},
	methods: {
    async request(form) {
      var response = await this.$store
        .dispatch(this.action, form)
     
      this.$emit('form-success', response)
    },
		async send(form) {
      this.$emit('form-submit')
			this.pending = true
			this.errors = {}
      if (this.isRecaptcha) {
        form.append('recaptcha', grecaptcha.getResponse())
      }

      try {
        await this.request(form)
      } catch(error) {
        if (error.hasOwnProperty('response')) {
          var response = error.response
          var data = response.data
          if (response.status == 422) {
            if (data.hasOwnProperty('errors')) {
              this.errors = data.errors
      				this.$emit('form-errors')
            }
          } else {            
            EventBus.$emit('error', error)
          }
          
          if (data.hasOwnProperty('error')) {
      			this.$emit('form-error', data.error)
          }
        } 
      }

      this.pending = false
      if (this.isRecaptcha) {
      	grecaptcha.reset()
      }
		},
	},
}