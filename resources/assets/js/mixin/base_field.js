import BaseField from './../components/fields/BaseField'

export default {
    components: {BaseField},
    props: ['form', 'errors'],
}