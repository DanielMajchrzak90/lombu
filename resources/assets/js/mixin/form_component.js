import LocaleMixin from './locale.js';

export default {
  mixins: [LocaleMixin],
	props: [  
		'component',
		'resource',
		'errors'
	],
  computed: {
    attributes() {
      return this.component.attributes
    },
  },
}