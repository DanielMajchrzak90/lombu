import FormComponentMixin from './form_component'

export default {
  mixins: [FormComponentMixin],
	data() {
		return {
			value: '',
		}
	},	
  watch: {
    value(value) {
      this.fillFormData(value)
    },
    resource: {
      deep: true,
      handler(resource) {
        if (this.resource[this.attributes.name] != this.value) {
          this.value = resource[this.attributes.name]
        }
      }
    }
  },
  methods: {
  	initValue() {
  		return this.value = this.resource[this.attributes.name] || ''
  	},
    fillFormData(value) {
      this.$set(this.resource, this.attributes.name, value)
      this.$emit('unset-form-data', {
        resource: this.attributes.name
      })
      this.$emit('fill-form-data', {
        name: this.attributes.name,
        value: value
      })
    }
  },
  mounted() {
  	this.fillFormData(this.initValue())
  }
}