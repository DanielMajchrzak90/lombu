{
  "components": [
    {
      "name": "card",
      "components": [
        {
          "name": "text-field",
          "attributes": {
            "name": "",
            "label": "",
            "id": ""
          }
        },
        {
          "name": "code-field"
        },
        {
          "name": "toggle-field",
          "components": [
            {
              "name": "text-field"
            },
            {
              "name": "code-field"
            }
          ]
        }
      ]
    }
  ]
}