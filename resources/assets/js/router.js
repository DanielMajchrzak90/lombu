import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import PageNotFound from './views/errors/PageNotFound.vue';
import Forbidden from './views/errors/Forbidden.vue';
import Unauthenticated from './views/errors/Unauthenticated.vue';
import FilesCkIndex from './views/panel/files/ck/Index.vue';

import store from './store/index'
import _ from 'lodash'

import {
  CHECK_AUTH,
} from './store/types.js'

import AuthRoutes from './routes/auth'
import PanelRoutes from './routes/panel'

const CK_FILES = require('./store/types/ck_files')
const AUTH = require('./store/types/auth')

const routes = [
  { 
  	path: "/404", 
  	component: PageNotFound 
  },
  { 
  	path: "/403", 
  	component: Forbidden 
  },
  { 
  	path: "/401", 
  	component: Unauthenticated,
  },
	{
	  name: 'home',
	  path: '/', 
    redirect: to => {
    	return '/' + to.params.locale + '/auth/login'
    },
		meta: { requiresGuest: true },
	},
 	AuthRoutes, 
 	PanelRoutes,  
  {
    name: 'files/ck',
    path: '/panel/files/ck',
    component: FilesCkIndex,
    meta: { 
      requiresAuth: true,
      async action(params) {
        await store.dispatch(CK_FILES.INDEX)    
      }, 
    },
  },
  { 
  	path: "/*", 
  	component: PageNotFound 
  },
];

_.map(routes, function(route) {
  route.path = '/:locale(pl|en)' + route.path
  
  return route
})

export const router = new VueRouter({ 
	mode: 'history', 
	routes: routes, 
  linkActiveClass: "active",
});

router.beforeEach(
  async (to, from, next) => {
    var locale = to.params.locale
    store.state.lang.setLocale(locale)
    
    await store.dispatch(AUTH.CHECK_AUTH)

    if (to.meta.requiresAuth) { 
      if (store.getters[AUTH.IS_AUTHENTICATED]) {
        next()
      } else {
        next({name: 'home', params: {locale}})
      }
    } else if (to.meta.requiresGuest) {
    	await store.dispatch(AUTH.CHECK_AUTH)

      if (!store.getters[AUTH.IS_AUTHENTICATED]) {
        next()
      } else {
        next({name: 'panel', params: {locale}})	        	
      }
    } else {
    	next()
    }
  }
)