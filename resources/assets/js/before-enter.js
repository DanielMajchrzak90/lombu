import store from './store/index'
import EventBus from './event-bus';

export default async (to, from, next) => { 
	$("#progressBar").parent().show()
	$("#progressBar").animate({width: '80%'});
 	for (let index = 0; index < to.matched.length; index++) {
    const match = to.matched[index]
		if (match.meta.action) {
		  try {
		  	await match.meta.action(to.params, to.query)
		  } catch (error) {
		  	EventBus.$emit('error', error)
		    break;
		  } 			
		}
  }		
	$("#progressBar").animate({width: '100%'}, 100, function() {
		$(this).parent().hide()
		$(this).css({width: '0%'})
	});

	return next()
}
