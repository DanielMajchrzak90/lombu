export default {
  // The key format should be: 'locale.filename'.
  'en.messages': require('../../lang/en/messages.php'),
  'en.actions': require('../../lang/en/actions.php'),
  'en.passwords': require('../../lang/en/passwords.php'),
  'en.auth': require('../../lang/en/auth.php'),
  'pl.messages': require('../../lang/pl/messages.php'),
  'pl.actions': require('../../lang/pl/actions.php'),
  'pl.passwords': require('../../lang/pl/passwords.php'),
  'pl.auth': require('../../lang/pl/auth.php'),
}