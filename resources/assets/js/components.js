import Vue from 'vue';
import MyForm from './components/MyForm';
import BaseField from './components/fields/BaseField'
import TextEditorField from './components/fields/TextEditorField'
import VueTextEditorField from './components/fields/VueTextEditorField'
import CodeEditorField from './components/fields/CodeEditorField'
import TextField from './components/fields/TextField'
import ToggleFieldComponent from './components/fields/ToggleFieldComponent'
import SelectField from './components/fields/SelectField'
import SelectMultipleField from './components/fields/SelectMultipleField'
import DatepickerField from './components/fields/DatepickerField'
import AutoCompleteField from './components/fields/AutoComplete/Field'
import FilesField from './components/fields/FilesField'
import NestedComponent from './components/fields/Nested/ItemsComponent'
import RowComponent from './components/fields/RowComponent'
import Pagination from './components/Pagination'
import Breadcrumb from './components/Breadcrumb'
import MyTable from './components/tables/Table'
import Modal from './components/Modal'
import MiniTable from './components/mini_table/MiniTable'

import ErrorMessage from './components/fields/ErrorMessage'

import FormPage from './components/pages/Form'
import LombardFormPage from './components/pages/lombard/Form'
import LombardCardPage from './components/pages/lombard/Card'
import AuthCard from './components/pages/AuthCard'

import DestroyButton from './components/DestroyButton'
import Container from './components/Container'

Vue.component('my-form', MyForm)
Vue.component('base-field', BaseField)
Vue.component('text-editor-field', TextEditorField)
Vue.component('vue-text-editor-field', VueTextEditorField)
Vue.component('text-field', TextField)
Vue.component('error-message', ErrorMessage)
Vue.component('form-page', FormPage)
Vue.component('lombard-card-page', LombardCardPage)
Vue.component('lombard-form-page', LombardFormPage)
Vue.component('destroy-button', DestroyButton)
Vue.component('auth-card', AuthCard)
Vue.component('toggle-field-component', ToggleFieldComponent)
Vue.component('code-editor-field', CodeEditorField)
Vue.component('row-component', RowComponent)
Vue.component('select-field', SelectField)
Vue.component('select-multiple-field', SelectMultipleField)
Vue.component('datepicker-field', DatepickerField)
Vue.component('auto-complete-field', AutoCompleteField)
Vue.component('files-field', FilesField)
Vue.component('nested-component', NestedComponent)
Vue.component('pagination', Pagination)
Vue.component('breadcrumb', Breadcrumb)
Vue.component('my-table', MyTable)
Vue.component('container', Container)
Vue.component('modal', Modal)
Vue.component('mini-table', MiniTable)

Vue.directive('tooltip', {
	unbind: function(el, binding) {
	  $(el).tooltip('dispose')
	},	
	bind: function(el, binding) {
	  $(el).tooltip({
	       title: binding.value,
	       placement: binding.arg || 'top',
	       trigger: 'hover'             
	   })
	},
})