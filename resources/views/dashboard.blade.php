<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Lombu</title>
    <link href="{{ mix('css/app.css')}}" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div style="position: fixed; left:0px; right:0px; top: 0px; z-index: 1000">
      <div class="progress" style="display: none; height: 3px">
        <div 
          id="progressBar"
          class="progress-bar" 
          role="progressbar" 
          style="width: 0%;"></div>
      </div>
    </div>
    <div id="app">
    </div>
    <input 
      type="hidden" 
      id="recaptcha-sitekey-input" 
      value="{{ config('captcha.sitekey') }}">
    <input 
      type="hidden" 
      id="app-name" 
      value="{{ config('app.name') }}">
    <script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.3.3/ace.js"></script>
    <script src="{{ asset('ckeditor_full/ckeditor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>   
  </body>
</html>