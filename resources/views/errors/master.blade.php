<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$exception->getMessage()}} - Lombu</title>
    <link href="{{ mix('css/app.css')}}" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="offset-md-3 col-md-6">
      <div class="jumbotron mt-5 text-center">
        <h1 class="display-4">@yield('code')</h1>
        <p class="lead">{{$exception->getMessage()}}</p>
        <a class="btn btn-primary btn-lg" href="{{route('home')}}" role="button">{{__('messages.back')}}</a>
      </div>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>   
  </body>
</html>