<table 
	cellpadding="5" 
	style="border-collapse: collapse;font-size:11px;width:100%">
  <thead>
    <tr>
      <th 
      	style="border: 0.5px solid #000"
      	scope="col">{{__('messages.name')}}</th>
      <th 
      	style="border: 0.5px solid #000"
      	scope="col">{{__('messages.market_price')}}</th>
    </tr>
  </thead>
  <tbody>
  	@foreach ($products as $product)
	    <tr>
	      <td 
      	style="border: 0.5px solid #000">{{$product->name}}</td>
	      <td
      	style="border: 0.5px solid #000">{{$product->market_price}}</td>
	    </tr>	
		@endforeach
  </tbody>
</table>