<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			*{ font-family: DejaVu Sans !important;}
			.page_break { page-break-before: always; }
			body {
				font-size: 14px;
			}
		</style>
		<title>{{ trans('messages.agreement_pattern_preview') }}</title>
	</head>
	<body>
		{!! $content !!}
	</body>
</html>