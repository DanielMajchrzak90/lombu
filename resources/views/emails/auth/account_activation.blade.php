@component('mail::message')
# {{ trans('auth.registration') }}

@component('mail::button', ['url' => route('email.confirmation', [$code]), 'color' => 'blue'])
{{ trans('auth.activate_account')  }}
@endcomponent

{{ trans('auth.thanks') }},<br>
{{ config('app.name') }}
@endcomponent