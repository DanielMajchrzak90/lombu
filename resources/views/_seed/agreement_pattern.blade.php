<p><img alt="" src="{{asset('images/default_logo.png')}}" style="position: absolute; width: 174px; height: 130px;" /></p>

<h2 style="text-align: center;">&nbsp;</h2>

<h2 style="text-align: center;"><strong>UMOWA LOMBARDOWA</strong></h2>

<p>&nbsp;</p>

<p><span style="font-size:14px;">Zawarta w dniu :created_at w Dąbrowa G&oacute;rnicza pomiędzy:</span></p>

<ol>
	<li><span style="font-size:14px;">Loombard z siedzibą przy ul. Kasprzaka 12 reprezentowaną przez Macieja Tomaszczyka zam. w Katowicach przy ul. Mariackiej 32 zwanym dalej właścicielem lombardu, a</span></li>
	<li><span style="font-size:14px;">:full_name o numerze pesel :pesel legitymującym się dokumentem :document&nbsp;zwanym dalej Pożyczkobiorcą.</span></li>
</ol>

<p style="text-align: center;"><span style="font-size:14px;"><strong>&sect; 1.</strong></span></p>

<p><span style="font-size:14px;">Właściciel lombardu pożycza Pożyczkobiorcy kwotę :amount zł.&nbsp;(słownie :word_amount złotych).</span></p>

<p style="text-align: center;"><span style="font-size:14px;"><strong>&sect; 2.</strong></span></p>

<p><span style="font-size:14px;">Zabezpieczenie pożyczki stanowi pozostawiona przez Pożyczkobiorcę Właścicielowi lombardu rzecz:<br />
:products</span></p>

<p style="text-align: center;"><span style="font-size:14px;"><strong>&sect; 3.</strong></span></p>

<ol>
	<li><span style="font-size:14px;">Pożyczkobiorca jest zobowiązany zwr&oacute;cić pożyczoną kwotę określoną w&sect; 1 wraz z prowizją do dnia :end_at</span></li>
	<li><span style="font-size:14px;">Prowizja wynosi :provision zł. (słownie :word_provision złotych).</span></li>
</ol>

<p style="text-align: center;"><span style="font-size:14px;"><strong>&sect; 4.</strong></span></p>

<p><span style="font-size:14px;">W przypadku niedotrzymania terminu określonego w&sect; 3 Pożyczkobiorca upoważnia Właściciela lombardu do dokonania sprzedaży w imieniu własnym, ale na rachunek pożyczkobiorcy, rzeczy określonej w&sect; 1 i do zaspokojenia swojej wierzytelności z uzyskanej sumy.</span></p>

<p style="text-align: center;"><br />
<span style="font-size:14px;"><strong>&sect; 5.</strong></span></p>

<table align="right" border="0" cellpadding="1" cellspacing="1" style="width:100%;">
	<tbody>
		<tr>
			<td>
			<p><span style="font-size:14px;">Właściciel lombardu</span></p>

			<p><span style="font-size:14px;">...............................................</span></p>
			</td>
			<td>
			<p style="text-align: right;"><span style="font-size:14px;">Pożyczkobiorca</span></p>

			<p style="text-align: right;">................................................</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>
