{{__('messages.agrrement_pattern_help')}}
<ul>
	@foreach (explode(',', \App\Models\AgreementPattern::FIELDS) as $field)
	<li class="">:{{$field}} - {{__('messages.'.$field)}}</li>
	@endforeach
</ul>
<div class="clear"></div>